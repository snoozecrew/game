#pragma once
#include <keyState.h>
#include <PrivateBehaviour.h>

class Game;

class PlayerControlSwitch : public PrivateBehaviour
{

public:

	PlayerControlSwitch(Game*, PrivateBehaviour*, PrivateBehaviour*);

	void update(double);

private:

	Game * gRefr;
	Keystate currKeys;

	PrivateBehaviour* b1;
	PrivateBehaviour* b2;

	bool b1Active = true;

	double ticker = 0.0;
};

