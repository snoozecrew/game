#include "DreameaterTerrainTextures.h"




DreameaterTerrainTextures::DreameaterTerrainTextures()
{
	bases[0] = fiLoadTexture("..\\..\\sceneryassets\\Textures\\World\\Grass_Ground.png");
	normals[0] = fiLoadTextureNormal("..\\..\\sceneryassets\\Textures\\World\\grass01_n.jpg");
	specs[0] = fiLoadTexture("..\\..\\sceneryassets\\Textures\\World\\splat.png");
	normalInvertY[0] = 1;

	bases[1] = fiLoadTexture("..\\..\\sceneryassets\\Textures\\World\\Mud_Ground.png");
	normals[1] = fiLoadTextureNormal("..\\..\\sceneryassets\\Textures\\World\\Mud_n1.jpg");

	splat = fiLoadTexture("..\\..\\sceneryassets\\Textures\\World\\splat.png");
}

