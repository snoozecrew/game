#include "EnemyPool.h"
#include <Scene.h>
#include "Player.h"

EnemyPool::EnemyPool()
{
}


EnemyPool::~EnemyPool()
{
}

void EnemyPool::addMinion(AttackableObject * x)
{
	//check if at max

	if (currNumOfMinions < MAX_MINIONS) 
	{
		for (int count = 0; count < MAX_MINIONS; count++)
		{
			if (minions[count] == nullptr)
			{	
			
				
				minions[count] = x;
				currNumOfMinions++;

				return;
			}
		}
	}
}



void EnemyPool::removeMinion(AttackableObject * x)
{
	for (int count = 0; count < MAX_MINIONS; count++)
	{
		if (minions[count] == x)
		{
			minions[count]->setPos(glm::vec3(0));
			minions[count] = nullptr;
			sceneTarget->removeGameObject(x->getHealthBar());
			sceneTarget->removeGameObject(x); 
			currNumOfMinions--;

			return;
		}
	}
}

//pass position, facing direction, and int for type of attack
std::vector<AttackableObject*> EnemyPool::getAttackableMinions(glm::vec3 pos, glm::vec3 facing, int attackType)
{
	std::vector<AttackableObject*> nearby;
	for (int count = 0; count < MAX_MINIONS; count++)
	{
		if (minions[count] != nullptr)
		{
			if (inAttackArea(pos, facing, attackType, count))
			{
				nearby.push_back(minions[count]);
			}
		}
	}
	return nearby;
}

//receives the object that is attacking's details (not limited to player)
bool EnemyPool::inAttackArea(glm::vec3 pos, glm::vec3 facing, int attackType, int minionIndex)
{
	//Calc area for this attack

	//In radians
	float spread = attackSpreads[attackType];

	//Check that it is in radius of player, then check if within spread
	if (inAttackRadius(pos, minions[minionIndex]->getPos(), attackRadii[attackType])) {

		glm::vec3 difference = minions[minionIndex]->getPos() - pos;
		float angleToPlayer = atan2(difference.x, difference.z);
		
		//check if within spread
		if (angleToPlayer > facing.y - spread / 2 && angleToPlayer < facing.y + spread / 2) {

			return true;
		}
	}
	return false;
}


bool EnemyPool::inAttackRadius(glm::vec3 centre, glm::vec3 query, float radius)
{
	float length = glm::length(query - centre);
	if(length > radius) return false;

	return true;
}
