#pragma once
#include <Terrain.h>
#include "DreamTerrainShaders.h"

class DreamTerrain : public Terrain

{
public:
	DreamTerrain(VAOData *, TerrainTextureData * t);
	~DreamTerrain();

	void draw(RenderSettings*);
	void update(double);
private:
	DreamTerrainShaders * drTerrShaders = nullptr;
	glm::vec3* circles;
};

