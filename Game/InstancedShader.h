#pragma once
#include "DreameaterShader.h"
#include <InstancedShaders.h>
class DreamInstancedShader : public DreameaterShader, public InstancedShaders
{
public:
	static DreamInstancedShader * getInstance();
	static void forceRebuild();
private:
	static DreamInstancedShader * thisPointer;
	DreamInstancedShader();
	~DreamInstancedShader();

	void setup();

	std::string iDreamVertFilename = "Shaders/vertex_shader_i_dream.shader";
	std::string iDreamFragFilename = "Shaders/fragment_shader_i_dream.shader";
};

