#include "DreamInstanced.h"

#include "InstancedShader.h"

DreamInstanced::DreamInstanced(InstancedVAOData* iData) :InstancedObject3D(iData)
{
	
}


DreamInstanced::~DreamInstanced()
{
}


void DreamInstanced::update(double x)
{
	//nout really

	updateBehaviours(x);
}

void DreamInstanced::draw(RenderSettings* rs)
{
	defaultShaders = DreamInstancedShader::getInstance();

	InstancedObject3D::draw(rs);
}

