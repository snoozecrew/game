#include "TrackColouredArea.h"
#include "DreamTerrainShaders.h"
#include "EntityShaders.h"
#include "InstancedShader.h"
#include "EnemySpawner.h"
#include <Scene.h>

TrackAndReportColouredArea::TrackAndReportColouredArea(GameObject* x, EnemySpawner* e, Cara * c) : cara(c)
{
	//player for colour
	player = x;
	//spawner to control spawning dependent on captured area
	spawner = e;

	barTex = fiLoadTexture("Textures/loadBar.png");
	barRes.x = getW();
	barRes.y = getH();
	progressTex = fiLoadTexture("Textures/ProgressFocal.png");
}


TrackAndReportColouredArea::~TrackAndReportColouredArea()
{
}

void TrackAndReportColouredArea::update(double x)
{
	ticker += x;
	delta = x;


	recordUnlockedAreas();
	

	if (ticker > MS_BETWEEN_UPDATES)
	{
		ticker = 0.0;
		recordPlayerPosition();
		recordCara();

		if (positions.size() > MAX_STORED_POSITIONS)
		{
			removeFirstEntry();
		}

		glUseProgram(EntityShaders::getInstance()->getProgramID());
		EntityShaders::getInstance()->setCircles(positions);
		EntityShaders::getInstance()->setFocalPoints(focalPoints);
		EntityShaders::getInstance()->setCaraPos(caraPos);

		glUseProgram(DreamTerrainShaders::getInstance()->getProgramID());
		DreamTerrainShaders::getInstance()->setCircles(positions);
		DreamTerrainShaders::getInstance()->setFocalPoints(focalPoints);
		DreamTerrainShaders::getInstance()->setCaraPos(caraPos);

		glUseProgram(DreamInstancedShader::getInstance()->getProgramID());
		DreamInstancedShader::getInstance()->setCircles(positions);
		DreamInstancedShader::getInstance()->setFocalPoints(focalPoints);
		DreamInstancedShader::getInstance()->setCaraPos(caraPos);
	}

	for (int count = 0; count < actPoints; count++)
	{
		if (timers[count] != nullptr)
		{
			timers[count]->setProgress(timeTillUnlocked[count]);
		}
	}
}

void TrackAndReportColouredArea::addFocalPoint(glm::vec3 f)
{
	if (actPoints < MAX_FOCAL_POINTS)
	{
		glm::vec4 p = glm::vec4(f, 3);
		focalPoints.push_back(p);

		timers[actPoints] = new FloatingHealthBar(barTex, progressTex, barRes, 0.95f,  0.8f, true);
		timers[actPoints]->setPos(f + glm::vec3(0, 3, 0));

		sceneTarget->addTransparentGameObject(timers[actPoints]);

		timers[actPoints]->setup(timeTillUnlocked[actPoints]);

		timers[actPoints]->setScale(glm::vec3(0.016f));
		actPoints++;
	}
}


void TrackAndReportColouredArea::recordPlayerPosition()
{
	glm::vec3 pos = player->getPos();
	positions.push_back(pos);

}

void TrackAndReportColouredArea::recordUnlockedAreas()
{
	glm::vec3 pos = player->getPos();

	for (int count = 0; count < actPoints; count++)
	{
		focalPos = glm::vec3(focalPoints[count].x, focalPoints[count].y, focalPoints[count].z);

		//unlocked if true
		if (timeTillUnlocked[count] < 0.0f)
		{
			if (focalPoints[count].w < affectingRadiuses[count])
			{
				//increase radius of coloured area by a bit
				focalPoints[count].w += delta/300;
			}
			//stop spawning enemies from the spawn waypoints
			spawner->focalPointUnlocked(count);
		}
		else if (glm::length(pos - focalPos) < unlockingRadiuses[count])
		{
			//player is inside the unlocking radius
			timeTillUnlocked[count] -= delta;
			std::cout << " In focal area" << timeTillUnlocked[count]<<"\n";
		}
		
	}
}

void TrackAndReportColouredArea::recordCara()
{
	caraPos = cara->getPos();
}

void TrackAndReportColouredArea::removeFirstEntry()
{
	positions.erase(positions.begin());
}