#include "KeyboardMove.h"
#include <Game.h>


KeyboardMove::KeyboardMove(Game* refr) :gameRefr(refr)
{

}


KeyboardMove::~KeyboardMove()
{
}

void KeyboardMove::update(double deltaTime)
{
	if (active) {

		currKeys = gameRefr->getKeys();

		glm::vec3 currPos = target->getPos();

		//Accelerate
		if (currKeys & Keys::R)
		{
			additive += glm::vec3(0, deltaTime*speed, 0);
		}
		if (currKeys & Keys::F)
		{
			additive += glm::vec3(0, -deltaTime * speed, 0);
		}
		if (currKeys & Keys::Left)
		{
			additive += glm::vec3(deltaTime*speed, 0, 0);
		}
		if (currKeys & Keys::Right)
		{
			additive += glm::vec3(-deltaTime * speed, 0, 0);
		}
		if (currKeys & Keys::Up)
		{
			additive += glm::vec3(0, 0, deltaTime * speed);
		}
		if (currKeys & Keys::Down)
		{
			additive += glm::vec3(0, 0, -deltaTime * speed);
		}

		rotAdd = target->getRotMat() * glm::vec4(additive, 0);

		target->setPos(currPos + rotAdd);
		additive *= pow(decay, deltaTime);
	}
}