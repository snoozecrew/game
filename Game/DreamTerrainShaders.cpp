#include "DreamTerrainShaders.h"


DreamTerrainShaders * DreamTerrainShaders::thisPointer = nullptr;

DreamTerrainShaders::DreamTerrainShaders()
{
	programID = setupShaders(drVertTerr, drFragTerr);
	setup();
	DreameaterShader::setup(programID);
	this;
}


DreamTerrainShaders::~DreamTerrainShaders()
{

}

DreamTerrainShaders * DreamTerrainShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new DreamTerrainShaders();
	}
	return thisPointer;
}

void DreamTerrainShaders::forceRebuild()
{
	delete thisPointer;
	thisPointer = new DreamTerrainShaders();

}

void DreamTerrainShaders::setup()
{

	TerrainShaders::setup();
	
}


