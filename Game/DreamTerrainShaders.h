#pragma once
#include <TerrainShaders.h>
#include "DreameaterShader.h"

class DreamTerrainShaders : public TerrainShaders , public DreameaterShader
{
public:
	static DreamTerrainShaders * getInstance();
	static void forceRebuild();
	void newFrame() { first = true; }
private:

	static DreamTerrainShaders * thisPointer;
	DreamTerrainShaders();
	~DreamTerrainShaders();
	void setup();

	std::string drVertTerr = "Shaders/vertex_shader_terr_dream.shader";
	std::string drFragTerr = "Shaders/fragment_shader_terr_dream.shader";


};

