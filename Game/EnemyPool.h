#pragma once
#include <vector>
#include <glm-0.9.9.2/glm/glm.hpp>
#include <PublicBehaviour.h>
#include "AttackableObject.h"
#include "AttackTypes.h"

class Player;

class EnemyPool : public PublicBehaviour
{
public:
	EnemyPool();
	~EnemyPool();

	const static int MAX_MINIONS = 15;

	void addMinion(AttackableObject *);

	//Remove minion will delete it entirely
	void removeMinion(AttackableObject *);

	int getCurrNumOfMinions() { return currNumOfMinions; }

	//Array of minions within the area of effect of damage
	std::vector<AttackableObject*> getAttackableMinions(glm::vec3, glm::vec3, int);

	void update(double) {}

private:

	int currNumOfMinions = 0;

	bool inAttackArea(glm::vec3, glm::vec3, int, int);

	bool inAttackRadius(glm::vec3 centre, glm::vec3 query, float radius);

	AttackableObject* minions[MAX_MINIONS] = { nullptr };

	const float NEARBY_DISTANCE = 1.0f;

};

