#pragma once
#include <PublicBehaviour.h>
#include <glm-0.9.9.2/glm/glm.hpp>
#include <GameObject.h>
#include <FloatingHealthBar.h>
#include "Cara.h"
class EnemySpawner;

class TrackAndReportColouredArea : public PublicBehaviour
{
public:
	TrackAndReportColouredArea(GameObject*, EnemySpawner*, Cara*);
	~TrackAndReportColouredArea();
	
	void update(double);

	void addFocalPoint(glm::vec3);

	static const int MAX_FOCAL_POINTS = 3;

private:
	void removeFirstEntry();

	double ticker = 0.0;
	GameObject * player;
	EnemySpawner * spawner;

	static const int MS_BETWEEN_UPDATES = 10;
	void recordPlayerPosition();
	void recordUnlockedAreas();

	void recordCara();
	const int MAX_STORED_POSITIONS = 80;

	std::vector<glm::vec3> positions;

	glm::vec3 caraPos;
	int actPoints = 0;
	std::vector<glm::vec4> focalPoints;

	int unlockingRadiuses[MAX_FOCAL_POINTS] = { 8, 8,8 };

	int affectingRadiuses[MAX_FOCAL_POINTS] = { 110, 110, 150 };

	//time in seconds the player has to spend in each area to unlock it
	float timeTillUnlocked[MAX_FOCAL_POINTS] = { 18000.0f, 25000.0f, 30000.0f };

	FloatingHealthBar * timers[MAX_FOCAL_POINTS] = { nullptr };

	glm::vec3 focalPos;

	double delta;

	GLuint barTex;
	GLuint progressTex;

	glm::vec2 barRes;
	Cara * cara;
};

