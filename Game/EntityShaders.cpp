#include "EntityShaders.h"

EntityShaders * EntityShaders::thisPointer = nullptr;

EntityShaders::EntityShaders()
{
	programID = setupShaders(vertexFilename, fragFilename);
	setup();
}


EntityShaders::~EntityShaders()
{
}

void EntityShaders::forceRebuild()
{
}


EntityShaders* EntityShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new EntityShaders();
	}
	return thisPointer;
}

void EntityShaders::setup()
{
	DefaultShaders::setup();

	DreameaterShader::setup(DefaultShaders::programID);
}