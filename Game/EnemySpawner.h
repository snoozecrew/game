#pragma once
#include <PublicBehaviour.h>
#include "EnemyMinion.h"
#include "EnemyPool.h"
#include <VAOData.h>
#include <Waypoint.h>
#include <vector>

class Scene;

class EnemySpawner : public PublicBehaviour
{
public:
	EnemySpawner(Scene*,EnemyPool*);
	~EnemySpawner();

	void update(double);
	void setup(VAOData * v, AttackableObject* newPlayer);

	std::vector<glm::vec3> MINION_SPAWN_POINTS;

	void focalPointUnlocked(int i) { unlock[i] = true; }

private:

	void spawnEnemy(int level, glm::vec3 pos);

	void inSpawnerRadius(AttackableObject*);

	int randomXInSpawnRange();
	int randomZInSpawnRange();

	Scene * scene;
	AttackableObject * player;
	EnemyPool * pool;

	VAOData* minionVAO;

	glm::vec3 minionSpawn1;
	glm::vec3 minionSpawn2;

	glm::vec3 playerPos;


	const int EXTRA_SPAWN_RADIUS = 16;
	const int TIME_BETWEEN_SPAWNS = 2000;
	const int TIME_BETWEEN_SPAWNER_SPAWNS = 2500;
	double timer = 0.0;
	double spawnPointTimer = 0.0;


	GLuint barTex;
	GLuint healthTex;
	glm::vec2 barDims;

	bool unlock[3] = { false };
};

