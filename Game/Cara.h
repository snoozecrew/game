#pragma once
#include <Object3D.h>
#include "AttackableObject.h"
#include <Light.h>
#include <glm-0.9.9.2/glm/glm.hpp>
#include <PhysicsComponent.h>

#define CARA_STILL 0
#define CARA_MOVING_TO_TARGET 1
#define CARA_CATCHING_UP_WITH_PLAYER 2
#define CARA_HEALING 3

class Cara :public Object3D
{
public:
	Cara(VAOData * caraModel, AttackableObject * player);
	~Cara();

	void update(double);

	Light * setupLight();
private:

	int state = 0;
	Light * caraLight = nullptr;
	AttackableObject* player;

	void startStill();
	void still();
	double stillTimer = 1300.0;

	void startMoving();
	void moving();

	void startCatchUp();
	void catchUp();

	void startHeal();
	void heal();
	double healingTime = 10.0f;

	void getFlutterPos();
	glm::vec3 getFlutterRot();
	const float msPerSwing = 40;
	const double zSwingDegs = 12.5f;
	const double xSwingDegs =3.5f;
	float right = false;
	float thisSwing = 0;
	float flutterHeight = 0.05f;
	float swingTime = 0.02f;
	float currSinSwing = 0.0f;

	float lastY = 0.0f;

	bool up = false;
	//movement this frame
	glm::vec3 movement = glm::vec3(0);
	
	//rotation this frame
	glm::vec3 newRot = glm::vec3(0);

	float height = 2.0f;

	const int maxDistPlayer = 25;

	float calculateHeight();
	glm::vec3 destinationPos;

	float MAX_SPEED_DISTANCE = 0.3f;
	float MAX_SPEED = 0.0022f;
	float FLY_HEIGHT = 1.9f;


	float healTime= 2.0f;
	double healTimer = 0.0f;

	float totalHealingPoints = 40;

	float healAngle = 0.0f;
	float healAnglePerSec = 12.6f;
	float healRadius = 3.8f;

	float flyHeight = 1.9f;

	float totalHeightIncrease = 1.9f;
	float startHeightDiff = 0.1f;

	float timeSinceLastHeal = 0.0f;

	float timeBetweenHeals = 11.0f;

	float targetHeight = 0.0f;

	int healAmount = 50;

	float passiveHealRadius = 10;

	double ticker = 0.0;

};

