#pragma once
#include "MainMenu.h"
#include "main.h"
#include <EngineWrapper.h>
#include "World.h"

int main(int argc, char* argv[])
{
	EngineWrapper * horizonsInstance = new EngineWrapper();

	horizonsInstance->initGL(argc, argv , "DreamEater");

	horizonsInstance->sortConfig();

	Scene * levelOne = new MainMenu();
	horizonsInstance->initializeGame(levelOne);

	horizonsInstance->addScene(new World());
	horizonsInstance->startGame();

	return 0;
}