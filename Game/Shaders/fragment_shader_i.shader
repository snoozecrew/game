#version 330

uniform sampler2D textureImage;
uniform sampler2D specularImage;
uniform sampler2D normalMap;

uniform int useNormalMap;
uniform int invertY;

//Uniforms that have to be unique to each surface

#define MAX_LIGHTS 16

uniform float materialShininess;
uniform vec3 cameraPos_World;
uniform int numLights;

uniform struct Light {
   vec4 position;
   vec3 colors;
   float attenuation;
   float ambientCoefficient;
   float coneAngle;
   vec3 coneDirection;
   mat4 finalLightMatrix;
} allLights[MAX_LIGHTS];

in packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;

} inputFragment;


mat3 calcedTBN;
vec3 calcedNormal;

// output packet
layout (location = 0) out vec4 fragmentColour;

vec3 applyLight(Light thisLight, vec3 baseColor, vec3 surfacePos, vec3 surfaceToCamera)
{
	float attenuation = 1.0f;
	vec3 lightPos = (thisLight.finalLightMatrix*thisLight.position).xyz;
	vec3 surfaceToLight;

	vec3 coneDir = normalize(thisLight.coneDirection);

	if (thisLight.position.w == 0.0f)
	{
		//Directional Light (all rays same angle, use position as direction)
		surfaceToLight = normalize((thisLight.position).xyz);
		attenuation = 1.0f;
	}
	else
	{
		//Point light
		surfaceToLight =  normalize(lightPos - (surfacePos));


		float distanceToLight = length(lightPos -( surfacePos));
		attenuation = 1.0 / (1.0f + thisLight.attenuation * pow(distanceToLight, 2));

		//Work out the Cone restrictions
		float lightToSurfaceAngle = degrees(acos(dot(-surfaceToLight, normalize(coneDir))));
		
		if (lightToSurfaceAngle > thisLight.coneAngle)
		{
			attenuation = 0.0;
		}
	}

	//Ambient light calculation
	vec3 ambient = thisLight.ambientCoefficient * baseColor.rgb * thisLight.colors;

	//Diffuse Light Calculation
	float diffuseCoefficient = max(0.0, dot(calcedNormal,normalize(surfaceToLight)));
	vec3 diffuse = diffuseCoefficient * baseColor.rgb * thisLight.colors;

	//Specular Light Calculation
	float specularCoefficient = 0.0;
	if (diffuseCoefficient > 0.0)
	{
		specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight,calcedNormal))), materialShininess);
	}

	vec3 specular = specularCoefficient * texture2D(specularImage, inputFragment.textureCoord).xyz * thisLight.colors;

	return ambient + attenuation * (diffuse+ specular); 
}

vec3 calculateNormal()
{
	//Sort the input so that the normal is between 1 and minus 1 instead of 0 and 1
	vec3 inputN = (texture2D(normalMap, inputFragment.textureCoord)).xyz;

	vec3 readIn = normalize(inputN);

	readIn.x = (2.0* inputN.r) - 1.0;
	readIn.y = invertY *((2.0* inputN.g) - 1.0);
	readIn.z = (2.0*inputN.b-1.0) ;

	readIn = inputFragment.TBN *readIn;

	//Before we use this normal, it needs to be translated according to which way the surface is facing
	//We can do that using a TBN Matrix. This was calculated in the vertex shader
	return readIn;
}


void main(void) {
	//get the base colour from the texture
	vec4 tempFragColor = texture2D(textureImage, inputFragment.textureCoord).rgba;

	//Support for objects with and without a normal map
	if (useNormalMap == 1)
	{  
		calcedTBN = inputFragment.TBN;
		calcedNormal = calculateNormal();
	}
	else
	{
		calcedNormal = inputFragment.normal;
		calcedTBN = mat3(1);
	}

	vec3 surfaceToCameraWorld = normalize((cameraPos_World) - (inputFragment.vert));

	vec3 tempColour = vec3(0.0, 0.0, 0.0);

	for (int count = 0; count < numLights; count++)
	{
		tempColour += applyLight(allLights[count], tempFragColor.xyz, inputFragment.vert, surfaceToCameraWorld);
	}


	vec3 gamma = vec3(1.0 / 2.2);

	vec4 final = vec4(pow(tempColour, gamma), tempFragColor.a);

	fragmentColour = final;
}


