#version 330 core
layout (points) in;
layout (triangle_strip, max_vertices = 8) out;

uniform vec4 color;//base color of the effect
uniform vec4 target; //base target of the effect

uniform float time; //time in ms since start

//the z position of the particles
float z = 0.0f;

//The magnitude of the effect
uniform float mag;

uniform vec4 posRelParent;

//The input packet from the vertex shader
in VS_OUT {
    vec4 color;
	vec4 position;
	mat4 finalMatrix;
	float angle;
} gs_in[];

out vec4 fColor;
out float intensity;

float  dimensions = 0.05f;
float  additive = 0.001f;

void main() {    
	
	//Pass through the intensity
	intensity = mag;
	
	z = ((sin(time/10)+1.0f) * mag/3) + mag;

	float calcDimension =dimensions + mag/22;

	float calcAngle = gs_in[0].angle +(3.1459) / 2;

	float calcDimensionY = calcDimension * cos(calcAngle);

	float calcDimensionX = calcDimension * sin(calcAngle);

	fColor = gs_in[0].color;

    gl_Position = 
		gs_in[0].finalMatrix* 
		(
		gs_in[0].position + 
		vec4( -calcDimensionX,  -calcDimensionY,0.0,  0.0)
		+posRelParent
		);   

    EmitVertex();   

	gl_Position =
		gs_in[0].finalMatrix*
		(
			gs_in[0].position +
			vec4(calcDimensionX,calcDimensionY, 0.0,  0.0)
			+ posRelParent
			);

	EmitVertex();

	gl_Position =
		gs_in[0].finalMatrix*
		(
			gs_in[0].position +
			vec4(-calcDimensionX, -calcDimensionY,(10*calcDimension)+z,  0.0)
			+ posRelParent
			);

	EmitVertex();
	gl_Position =
		gs_in[0].finalMatrix*
		(
			gs_in[0].position +
			vec4(calcDimensionX,calcDimensionY, (10*calcDimension)+z,  0.0)
			+ posRelParent
			);

	EmitVertex();

	gl_Position =
		gs_in[0].finalMatrix*
		(
			gs_in[0].position +
			vec4(0.0, 0.0, (calcDimension *12)+z, 0.0)
			+ posRelParent
			);

	EmitVertex();


    EndPrimitive();
}  