#version 330

uniform mat4 T; // Translation matrix
uniform mat4 S; // Scale matrix
uniform mat4 R; // Rotation matrix
uniform mat4 camera; // camera matrix
uniform vec4 posRelParent; // the position relative to the parent
uniform vec4 clipPlane; //The clipping Plane
uniform int usePlane; //PLane bool

					  // Input vertex packet
layout(location = 0) in vec4 position;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec4 tangent;
layout(location = 4) in vec4 bitangent;
layout(location = 8) in vec2 textureCoord;


// Output vertex packet
out packet{

	vec2 textureCoord;


vec3 normal;
vec3 vert;
mat3 TBN;


} outputVertex;

mat4 transform;
mat3 TBN;
vec4 pos;

void calculateTBN()
{
	mat3 normalMat = transpose(inverse(mat3(transform)));

	//vec3 T = normalize(vec3(transform*tangent));
	vec3 T = normalize(vec3(normalMat *tangent.xyz));

	vec3 N = normalize(vec3(normalMat* vec4(normal, 0.0).xyz));

	//vec3 B = normalize(vec3(transform* bitangent));
	vec3 B = normalize(vec3(normalMat *bitangent.xyz));

	TBN = mat3(T, B, N);


	outputVertex.TBN = TBN;
}

void main(void) {
	//Work out the transform matrix
	transform = T * R * S;

	pos = vec4(position.x, position.y, position.z, 1.0) + posRelParent;

	pos = transform * pos;




	if (usePlane == 1)
	{
		gl_ClipDistance[0] = dot(pos, clipPlane);
	}
	else
	{
		gl_ClipDistance[0] = 1;
	}
	pos = camera * pos;

	outputVertex.textureCoord = textureCoord;





	//Work out the normal for lighting
	mat3 normalMat = transpose(inverse(mat3(transform)));

	outputVertex.normal = normalize(normalMat* normal);

	outputVertex.vert = (transform *position).xyz;


	calculateTBN();


	//Work out the final pos of the vertex
	gl_Position = pos;
}