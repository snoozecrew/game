#version 330

#define MAX_TEXTURES 4
uniform sampler2D bases[MAX_TEXTURES];
uniform sampler2D normals[MAX_TEXTURES];
uniform sampler2D specs[MAX_TEXTURES];
uniform int inverts[MAX_TEXTURES];
uniform sampler2D splat;

//Uniforms that have to be unique to each surface

#define MAX_LIGHTS 16

uniform float materialShininess;
uniform vec3 cameraPos_World;
uniform int numLights;

uniform int invertY;

uniform struct Light {
   vec4 position;
   vec3 colors;
   float attenuation;
   float ambientCoefficient;
   float coneAngle;
   vec3 coneDirection;
   mat4 finalLightMatrix;
} allLights[MAX_LIGHTS];

in packet{

	vec2 textureCoord;
	vec2 tiledCoord;

	vec3 normal;
	vec3 vert;
	mat3 TBN;

} inputFragment;

vec3 specColour = vec3(1,0,0);
mat3 calcedTBN;
vec3 calcedNormal;
vec4 tempFragColor;

int predominantTex=0;

// output packet
layout (location = 0) out vec4 fragmentColour;

vec3 applyLight(Light thisLight, vec3 baseColor, vec3 surfacePos, vec3 surfaceToCamera)
{
	float attenuation = 1.0f;
	vec3 lightPos = (thisLight.finalLightMatrix*thisLight.position).xyz;
	vec3 surfaceToLight;

	vec3 coneDir = normalize(thisLight.coneDirection);

	if (thisLight.position.w == 0.0f)
	{
		//Directional Light (all rays same angle, use position as direction)
		surfaceToLight = normalize((thisLight.position).xyz);
		attenuation = 1.0f;
	}
	else
	{
		//Point light
		surfaceToLight =  normalize(lightPos - (surfacePos));


		float distanceToLight = length(lightPos -( surfacePos));
		attenuation = 1.0 / (1.0f + thisLight.attenuation * pow(distanceToLight, 2));

		//Work out the Cone restrictions
		float lightToSurfaceAngle = degrees(acos(dot(-surfaceToLight, normalize(coneDir))));
		
		if (lightToSurfaceAngle > thisLight.coneAngle)
		{
			attenuation = 0.0;
		}
	}

	//Ambient light calculation
	vec3 ambient = thisLight.ambientCoefficient * baseColor.rgb * thisLight.colors;

	//Diffuse Light Calculation
	float diffuseCoefficient = max(0.0, dot(calcedNormal,normalize(surfaceToLight)));
	vec3 diffuse = diffuseCoefficient * baseColor.rgb * thisLight.colors;

	//Specular Light Calculation
	float specularCoefficient = 0.0;
	if (diffuseCoefficient > 0.0)
	{
		specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight,calcedNormal))), materialShininess);
	}

	vec3 specular = specularCoefficient * specColour * thisLight.colors;// 

	return ambient + attenuation * (diffuse+ specular); 
}

void splatCalcs()
{
	//Read in splat
	vec4 splat = (texture2D(splat, inputFragment.textureCoord));

	//read in each base colour texture
	vec4 base0 = texture2D(bases[0], inputFragment.tiledCoord);
	vec4 base1 = texture2D(bases[1], inputFragment.tiledCoord);
	vec4 base2 = texture2D(bases[2], inputFragment.tiledCoord);
	vec4 base3 = texture2D(bases[3], inputFragment.tiledCoord);

	//Use an appropriate blend from each texture. use normal and spec maps for the most prevalent map
	//Black = base 0
	//red = base 1
	//green = base 2
	//blue = base 3
	//a = 0 then base 4
	float black = 1 - (splat.r + splat.g + splat.b); 

	base0 = base0 * black;
	base1 = base1 * splat.r;
	base2 = base2 * splat.g;
	base3 = base3 * splat.b;

	//frag colour combined = 
	tempFragColor = base0 + base1 + base2 + base3;


	//Do everything again for normal maps
	vec3 normal0 = texture2D(normals[0], inputFragment.tiledCoord).xyz;
	vec3 normal1 = texture2D(normals[1], inputFragment.tiledCoord).xyz;
	vec3 normal2 = texture2D(normals[2], inputFragment.tiledCoord).xyz;
	vec3 normal3 = texture2D(normals[3], inputFragment.tiledCoord).xyz;


	normal0 = normal0 * black;
	normal1 = normal1 * splat.r;
	normal2 = normal2 * splat.g;
	normal3 =  normal3 * splat.b;


	vec3 inputN = normal0 + normal1 + normal2 + normal3;
	//input = (texture2D(normals[0], inputFragment.tiledCoord)).xyz;

	//Normal map specific calculations
	vec3 readIn;
	readIn.x = (2.0* inputN.r) - 1.0;
	readIn.y = invertY *((2.0* inputN.g) - 1.0);
	readIn.z = (2.0*inputN.b - 1.0);

	//calcec normal combined = 
	calcedNormal = normalize(inputFragment.TBN * readIn);

	//read in spec maps
	vec3 spec0 =  texture2D(specs[0], inputFragment.tiledCoord).xyz;
	vec3 spec1 =  texture2D(specs[1], inputFragment.tiledCoord).xyz;
	vec3 spec2 =  texture2D(specs[2], inputFragment.tiledCoord).xyz;
	vec3 spec3 =  texture2D(specs[3], inputFragment.tiledCoord).xyz;

	spec0 = spec0 * black;
	spec1 = spec1 * splat.r;
	spec2 = spec2 * splat.g;
	spec3 = spec3 * splat.b;

	//Set spec colour
	specColour = spec0 + spec1 + spec2 + spec3;

}


void main(void) {
	//get the base colour from the texture
	splatCalcs();

	//Support for objects with and without a normal map

	vec3 surfaceToCameraWorld = normalize((cameraPos_World) - (inputFragment.vert));

	vec3 tempColour = vec3(0.0, 0.0, 0.0);

	for (int count = 0; count < numLights; count++)
	{
		tempColour += applyLight(allLights[count], tempFragColor.xyz, inputFragment.vert, surfaceToCameraWorld);
	}

	vec3 gamma = vec3(1.0 / 2.2);

	vec4 final = vec4(pow(tempColour, gamma), tempFragColor.a);

	fragmentColour = final;
}


