#version 400

#define MAX_TEXTURES 4
#define MAX_LIGHTS 16
#define CIRCLESMAX 80
#define FOCALSMAX 6



// output packet
layout(location = 0) out vec4 fragmentColour;

uniform sampler2D bases[MAX_TEXTURES];
uniform sampler2D normals[MAX_TEXTURES];
uniform sampler2D specs[MAX_TEXTURES];
uniform int inverts[MAX_TEXTURES];
uniform sampler2D splat;

uniform vec3 circlePos[CIRCLESMAX];
uniform vec3 caraPos;
uniform vec4 focalPosAndRad[FOCALSMAX];

//Uniforms that have to be unique to each surface
uniform float materialShininess;
uniform vec3 cameraPos_World;
uniform int numLights;

uniform int invertY;

uniform struct Light {
	vec4 position;
	vec3 colors;
	float attenuation;
	float ambientCoefficient;
	float coneAngle;
	vec3 coneDirection;
	mat4 finalLightMatrix;
} allLights[MAX_LIGHTS];

in packet{
	vec2 textureCoord;
vec2 tiledCoord;
vec3 normal;
vec3 vert;
mat3 TBN;
} inputFragment;

vec3 specColour = vec3(1, 0, 0);
mat3 calcedTBN;
vec3 calcedNormal;
vec4 tempFragColor;

float circleRadius = 5;
float caraRadius =2.0f;

float bwMix = 1.0f;


vec3 applyLight(Light thisLight, vec3 baseColor, vec3 surfacePos, vec3 surfaceToCamera)
{
	float attenuation = 1.0f;
	vec3 lightPos = (thisLight.finalLightMatrix*thisLight.position).xyz;
	vec3 surfaceToLight;

	vec3 coneDir = normalize(thisLight.coneDirection);

	if (thisLight.position.w == 0.0f)
	{
		//Directional Light (all rays same angle, use position as direction)
		surfaceToLight = normalize((thisLight.position).xyz);
		attenuation = 1.0f;
	}
	else
	{
		//Point light
		surfaceToLight = normalize(lightPos - (surfacePos));


		float distanceToLight = length(lightPos - (surfacePos));
		attenuation = 1.0 / (1.0f + thisLight.attenuation * pow(distanceToLight, 2));

		//Work out the Cone restrictions
		float lightToSurfaceAngle = degrees(acos(dot(-surfaceToLight, normalize(coneDir))));

		if (lightToSurfaceAngle > thisLight.coneAngle)
		{
			attenuation = 0.0;
		}
	}

	//Ambient light calculation
	vec3 ambient = thisLight.ambientCoefficient * baseColor.rgb * thisLight.colors;

	//Diffuse Light Calculation
	float diffuseCoefficient = max(0.0, dot(calcedNormal, normalize(surfaceToLight)));
	vec3 diffuse = diffuseCoefficient * baseColor.rgb * thisLight.colors;

	//Specular Light Calculation
	float specularCoefficient = 0.0;
	if (diffuseCoefficient > 0.0)
	{
		specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, calcedNormal))), materialShininess);
	}

	vec3 specular = specularCoefficient * specColour * thisLight.colors;// 

	return ambient + attenuation * (diffuse + specular);
}

void splatCalcs()
{
	//Read in splat
	vec4 splat = (texture2D(splat, inputFragment.textureCoord));

	//read in each base colour texture
	vec4 base0 = texture2D(bases[0], inputFragment.tiledCoord);
	vec4 base1 = texture2D(bases[1], inputFragment.tiledCoord);
	vec4 base2 = texture2D(bases[2], inputFragment.tiledCoord);
	vec4 base3 = texture2D(bases[3], inputFragment.tiledCoord);

	//Use an appropriate blend from each texture. use normal and spec maps for the most prevalent map
	//Black = base 0
	//red = base 1
	//green = base 2
	//blue = base 3
	//a = 0 then base 4
	float black = 1 - (splat.r + splat.g + splat.b);

	base0 = base0 * black;
	base1 = base1 * splat.r;
	base2 = base2 * splat.g;
	base3 = base3 * splat.b;

	//frag colour combined = 
	tempFragColor = base0 + base1 + base2 + base3;


	//Do everything again for normal maps
	vec3 normal0 = texture2D(normals[0], inputFragment.tiledCoord).xyz;
	vec3 normal1 = texture2D(normals[1], inputFragment.tiledCoord).xyz;
	vec3 normal2 = texture2D(normals[2], inputFragment.tiledCoord).xyz;
	vec3 normal3 = texture2D(normals[3], inputFragment.tiledCoord).xyz;


	normal0 = normal0 * black;
	normal1 = normal1 * splat.r;
	normal2 = normal2 * splat.g;
	normal3 = normal3 * splat.b;


	vec3 inputN = normal0 + normal1 + normal2 + normal3;
	//input = (texture2D(normals[0], inputFragment.tiledCoord)).xyz;

	//Normal map specific calculations
	vec3 readIn;
	readIn.x = (2.0* inputN.r) - 1.0;
	readIn.y = invertY * ((2.0* inputN.g) - 1.0);
	readIn.z = (2.0*inputN.b - 1.0);

	//calcec normal combined = 
	calcedNormal = normalize(inputFragment.TBN * readIn);

	//read in spec maps
	vec3 spec0 = texture2D(specs[0], inputFragment.tiledCoord).xyz;
	vec3 spec1 = texture2D(specs[1], inputFragment.tiledCoord).xyz;
	vec3 spec2 = texture2D(specs[2], inputFragment.tiledCoord).xyz;
	vec3 spec3 = texture2D(specs[3], inputFragment.tiledCoord).xyz;

	spec0 = spec0 * black;
	spec1 = spec1 * splat.r;
	spec2 = spec2 * splat.g;
	spec3 = spec3 * splat.b;

	//Set spec colour
	specColour = spec0 + spec1 + spec2 + spec3;

}

vec4 blackAndWhiteCalcs(vec4 colour)
{
	//Black and white effects for Dreameater
	vec3 lum = vec3(0.299, 0.587, 0.114);

	//the black and white version of tempFragColor
	vec3 blackAndWhite = mix(vec3(dot(colour.xyz, lum)), colour.xyz, 0.15);
	blackAndWhite *= 0.8f;

	float bw = 0.0f;
	float additive;

	float lengthFromCentre = 0.0f;
	//if the radius is smaller than the distance between the vert and the centre, mul = 0.
	//if the radius is bigger than the distance between the vert and the centre, mul > 0
	//the result of the following eqn is 0 if the point is outside of the circle, and (0< result < radius) radius if it is.

	for (int count2 = 0; count2 < CIRCLESMAX; count2++)
	{
		lengthFromCentre = length(inputFragment.vert - circlePos[count2]);
		additive = (circleRadius - (min(circleRadius, lengthFromCentre)));

		// make sure the mul value is definetly either 0, or bigger than 1


		bw += additive;


	}


	for (int count2 = 0; count2 < FOCALSMAX; count2++)
	{
		lengthFromCentre = length(inputFragment.vert - focalPosAndRad[count2].xyz);
		additive = focalPosAndRad[count2].w - (min(focalPosAndRad[count2].w, lengthFromCentre));

		// make sure the mul value is definetly either 0, or bigger than 1


		bw += additive;
	}

	lengthFromCentre = length(inputFragment.vert.xz - caraPos.xz);
	additive = (caraRadius - (min(circleRadius, lengthFromCentre))) * 0.75f;

	// make sure the mul value is definetly either 0, or bigger than 1


	bw += additive;

	bw = clamp(bw, 0.0001, 0.9999);


	return vec4(mix(blackAndWhite, colour.xyz, bw), colour.a);
}

void main(void) {

	vec3 surfaceToCameraWorld = normalize((cameraPos_World)-(inputFragment.vert));

	//get the base colour from the texture
	splatCalcs();

	tempFragColor = blackAndWhiteCalcs(tempFragColor);

	vec3 tempColour = vec3(0.0, 0.0, 0.0);

	for (int count = 0; count < numLights; count++)
	{
		tempColour += applyLight(allLights[count], tempFragColor.xyz, inputFragment.vert, surfaceToCameraWorld);
	}

	vec3 gamma = vec3(1.0 / 2.2);

	vec4 final = vec4(pow(tempColour, gamma), tempFragColor.a);

	fragmentColour = final;
}


