#version 330 core
out vec4 FragColor;

in vec4 fColor;
in float intensity;

float intenseMod = 0.65f;
float intensityMod = 0.89f;

void main()
{
	vec4 color = fColor;
	color.w += intensity*intenseMod;
	color*= intensityMod;
    FragColor = color;   
} 