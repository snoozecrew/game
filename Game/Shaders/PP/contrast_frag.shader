#version 330
uniform sampler2D textureImage;
uniform float contrastStrength;
// input packet

in packet{
	vec2 textureCoord;
} inputFragment;


// output packet
layout(location = 0) out vec4 fragmentColour;


void main(void) {
	vec4 tempFragmentColour = texture2D(textureImage, inputFragment.textureCoord);

	vec3 tempColour = (tempFragmentColour.xyz - 0.5) * (1 + contrastStrength) + 0.5;

	fragmentColour = vec4(tempColour, tempFragmentColour.a);
}