#version 330 core
layout (location = 0) in vec3 aPos;

out vec3 TexCoords;

uniform mat4 projection;
uniform mat4 view;
uniform vec4 clipPlane; //The clipping Plane
uniform int usePlane; //PLane bool
uniform float scale;

void main()
{

	if (usePlane == 1)
	{
		gl_ClipDistance[0] = dot(vec4(aPos* scale, 1.0), clipPlane);
	}
	else
	{
		gl_ClipDistance[0] = 1;
	}


    TexCoords = aPos;
    gl_Position =view * vec4(aPos* scale, 1.0);
}  