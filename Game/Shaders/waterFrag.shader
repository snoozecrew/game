#version 330

layout(location = 0) out vec4 fragmentColour;

uniform sampler2D reflectionTexture;
uniform sampler2D refractionTexture;
uniform sampler2D dudv;
uniform sampler2D normal;
uniform sampler2D depthMap;
uniform vec3 cameraPos;

#define MAX_LIGHTS 16
uniform int numLights;

uniform struct Light {
	vec4 position;
	vec3 colors;
	float attenuation;
	float ambientCoefficient;
	float coneAngle;
	vec3 coneDirection;
	mat4 finalLightMatrix;
} allLights[MAX_LIGHTS];


uniform float offset;

float waterDepth;

const float waveStrength = 0.02;
const float shinyness = 75.0;
const float opacity = 0.75;
const float reflectivity = 1.0;
const float normalDamper = 2.5;
const vec3 waterTint = vec3(0, 0.1, 0.4);

vec3 calcedNormal;

in packet{

	vec4 clipSpace;
	vec2 textureCoords;
	vec3 toCamera;
	vec3 vert;

} inputFragment;


vec3 applyLight(Light thisLight, vec3 baseColor, vec3 surfacePos, vec3 surfaceToCamera)
{
	float attenuation = 1.0f;
	vec3 lightPos = (thisLight.finalLightMatrix*thisLight.position).xyz;
	vec3 surfaceToLight;

	vec3 coneDir = normalize(thisLight.coneDirection);

	if (thisLight.position.w == 0.0f)
	{
		//Directional Light (all rays same angle, use position as direction)
		surfaceToLight = normalize((thisLight.position).xyz);
		attenuation = 1.0f;
	}
	else
	{
		//Point light
		surfaceToLight = normalize(lightPos - (surfacePos));


		float distanceToLight = length(lightPos - (surfacePos));
		attenuation = 1.0 / (1.0f + thisLight.attenuation * pow(distanceToLight, 2));

		//Work out the Cone restrictions
		float lightToSurfaceAngle = degrees(acos(dot(-surfaceToLight, normalize(coneDir))));

		if (lightToSurfaceAngle > thisLight.coneAngle)
		{
			attenuation = 0.0;
		}
	}

	//Ambient light calculation
	vec3 ambient = thisLight.ambientCoefficient * baseColor.rgb * thisLight.colors;

	//Diffuse Light Calculation
	float diffuseCoefficient = max(0.0, dot(calcedNormal, normalize(surfaceToLight)));
	vec3 diffuse = diffuseCoefficient * baseColor.rgb * thisLight.colors;

	//Specular Light Calculation
	float specularCoefficient = 0.0;
	if (diffuseCoefficient > 0.0)
	{
		specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, calcedNormal))), shinyness) * clamp(waterDepth / 20.0, 0.0, 1.0);
	}

	vec3 specular = specularCoefficient * vec3(1.0, 1.0, 1.0) * thisLight.colors;

	return ambient + attenuation * (diffuse + specular);
}

void main(void)
{
	//Normal Device Coords
	vec2 ndc =( (inputFragment.clipSpace.xy / inputFragment.clipSpace.w)/2)+0.5;

	//Make the reflection and refraction coords
	vec2 reflectCoords = vec2(ndc.x, -ndc.y);
	vec2 refractCoords = vec2(ndc.x, ndc.y);

	//Pass in please
	float near = 0.1;
	float far = 200000.0;

	//Sample the depth buffer of the refraction map
	float depth = texture2D(depthMap, refractCoords).r;

	//Calculate floor distance
	float floorDistance = 2.0* near * far / (far + near - (2.0 * depth - 1.0) *(far - near));

	//Calculate Water distance
	depth = gl_FragCoord.z;
	float waterDistance = 2.0* near * far / (far + near - (2.0 * depth - 1.0) *(far - near));

	waterDepth = floorDistance - waterDistance;

	//Get some distortion from the DUDV map
	vec2 distortedTexCoords = texture2D(dudv, vec2(inputFragment.textureCoords.x + offset, inputFragment.textureCoords.y)).rg *0.1;
	distortedTexCoords = inputFragment.textureCoords + vec2(distortedTexCoords.x, distortedTexCoords.y + offset);
	vec2 distortion = (texture2D(dudv, distortedTexCoords).rg* 2.0 - 1.0) * waveStrength *  clamp(waterDepth / 20.0, 0.0, 1.0);

	//Distort the coords
	reflectCoords += distortion;
	refractCoords += distortion;

	//Clamp (avoiding off-screen blur
	refractCoords = clamp(refractCoords, 0.001, 0.999);
	reflectCoords.x = clamp(reflectCoords.x, 0.001, 0.999);
	reflectCoords.y = clamp(reflectCoords.y, -0.999, -0.001);

	//Sample the FBO textures with the distored Coords
	vec3 reflect = texture2D(reflectionTexture, reflectCoords).xyz;
	vec3 refract = texture2D(refractionTexture, refractCoords).xyz;
	
	//Normalize the view Vector
	vec3 viewVector = normalize(inputFragment.toCamera);

	//Read the normal map and calculate the normal
	vec4 normalMapColour = texture2D(normal, distortedTexCoords);
	calcedNormal = vec3(normalMapColour.r*2.0 - 1.0, normalMapColour.b *normalDamper, normalMapColour.g *2.0 - 1.0);
	calcedNormal = normalize(calcedNormal);

	//Fesnel Effect calculations
	float refractiveFactor = dot(viewVector, calcedNormal);

	refractiveFactor = pow(refractiveFactor, reflectivity)* 0.5;
	
	float alpha = clamp(waterDepth/4.0, 0.0, 1.0);

	//Total preLighting colour
	vec4 preLightingColour =   vec4(mix(reflect, refract, refractiveFactor),alpha);

	////Lighting stuff
	vec3 surfaceToCameraWorld = normalize((cameraPos)-(inputFragment.vert));

	vec3 tempColour = vec3(0.0, 0.0, 0.0);

	for (int count = 0; count < numLights; count++)
	{
		tempColour += applyLight(allLights[count], preLightingColour.xyz, inputFragment.vert, surfaceToCameraWorld);
	}

	//Thismixes the "lit" texture and the standard one to simulate particles in the water reflecting light 
	fragmentColour = mix(vec4(tempColour, alpha),preLightingColour, opacity);
}