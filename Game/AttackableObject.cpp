#include "AttackableObject.h"
#include "EnemyMinion.h"


AttackableObject::AttackableObject(EnemyPool* p) : pool(p)
{
}


AttackableObject::~AttackableObject()
{
}

void AttackableObject::takeDamage(int damage)
{
	attackDelayTimer = ATTACK_DELAY;
	health -= damage;
}

int AttackableObject::getHealth() {

	return health;
}

FloatingHealthBar* AttackableObject::setupHealthBar(GLuint barTex, GLuint progTex, glm::vec2 barDim)
{
	healthBar = new FloatingHealthBar(barTex, progTex,barDim, 0.95f, 0.8f, true);
	
	healthBar->setConfig(thisConfig);
	healthBar->setup(health);

	healthBar->setScale(glm::vec3(0.016f));

	return healthBar;
}

void AttackableObject::update()
{
	healthBar->setPos(getPos() + glm::vec3(0, 2.1f, 0));
	healthBar->setProgress(health);
}