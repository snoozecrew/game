#pragma once
#include <Scene.h>
#include <GameObject.h>
#include "EnemyPool.h"
#include "TrackColouredArea.h"
#include <InputListener.h>
#include "Cara.h"

class World : public Scene
{
public:
	World();
	~World();

	void loadTextures() {}
	void load(Game * gameRefr);
	void run();

private:

	void loadPlayer();

	void loadEnemySpawner();

	void loadEnemy();

	void loadLighting();
	
	void loadWorld();

	void loadInit();

	void loadCamera();

	void loadRiver();

	void loadGUI();

	void loadPP();

	void addPlayerBehaviours();

	void loadColourScripts();

	void loadCara();

	void loadEnemyPool();


	GameObject * player;

	AttackableObject * playerAsAO;


	InputListener * playerAsInputListener;

	Camera * gameCamera = nullptr;

	Cara * cara;

	EnemyPool * minionPool = nullptr;

	EnemySpawner* enemySpawner = nullptr;

	TrackAndReportColouredArea * trackBehaviour;
};

