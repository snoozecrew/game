#include "DreameaterShader.h"
#include "TrackColouredArea.h"
void DreameaterShader::setup(int newProgramID)
{
	programIDD = newProgramID;
	locBWmix = glGetUniformLocation(programIDD, "bwMix");

	for (int count = 0; count < MAX_CIRCLES; count++)
	{
		locCircles[count] = glGetUniformLocation(programIDD, ("circlePos[" + std::to_string(count) + "]").c_str());
	}

	for (int count = 0; count < TrackAndReportColouredArea::MAX_FOCAL_POINTS; count++)
	{
		locFocals[count] = glGetUniformLocation(programIDD, ("focalPosAndRad[" + std::to_string(count) + "]").c_str());
	}
	locCara = glGetUniformLocation(programIDD, "caraPos");
}
void DreameaterShader::setBwMix(float x)
{
	glUniform1f(programIDD, x);
}

void DreameaterShader::setCaraPos(glm::vec3 p)
{
	glUniform3fv(locCara,1, (GLfloat*) &p);
}

void DreameaterShader::setCircles(std::vector<glm::vec3> circles)
{
	for (int count = 0; count < circles.size(); count++)
	{
		glUniform3fv(locCircles[count], 1, (GLfloat*)&circles[count]);
	}

}
void DreameaterShader::setFocalPoints(std::vector<glm::vec4> points)
{
	for (int count = 0; count < TrackAndReportColouredArea::MAX_FOCAL_POINTS; count++)
	{
		if (count >= points.size()) break;
		glUniform4fv(locFocals[count], 1, (GLfloat*)&points[count]);
	}
}
