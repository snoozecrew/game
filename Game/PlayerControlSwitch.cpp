#include "PlayerControlSwitch.h"
#include <keyState.h>
#include <Game.h>



PlayerControlSwitch::PlayerControlSwitch(Game* g, PrivateBehaviour* behaviour1, PrivateBehaviour* behaviour2)
{
	behaviour1->setActive(true);
	behaviour2->setActive(false);

	b1 = behaviour1;
	b2 = behaviour2;

	b1Active = true;

	gRefr = g;
}


void PlayerControlSwitch::update(double delta) {

	currKeys = gRefr->getKeys();

	if (active)
	{
		//Switch between flight or normal
		if (currKeys & Keys::x) {

			b1->setActive(!b1Active);
			b2->setActive(b1Active);
			b1Active = !b1Active;

			ticker = 300.0;
		}
	}

	if (ticker > 0.1) {
		ticker -= delta; active = false;
	}
	else
	{
		active = true;
	}
}