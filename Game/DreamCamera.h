#pragma once
#include <Camera.h>
#include <glm-0.9.9.2/glm/glm.hpp>
#include <glm-0.9.9.2/glm/gtx/vector_angle.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#define _USE_MATH_DEFINES
#include <math.h>


class DreamCamera : public Camera
{
public:
	DreamCamera(GameObject*);
	~DreamCamera();
	void update(double delta, int mouseX, int mouseY, bool playing);

	glm::mat4 getLookAtMatrix(bool invertY, glm::vec3 pos);
	
private:

	glm::vec3 offset = glm::vec3(0);

	glm::vec3 playerPos = glm::vec3(0);

	glm::vec3 posTrackRelRottarget = glm::vec3(0);

	glm::vec3 lookingAtConst = glm::vec3(0.0f, -5.0f, 0.0f);

	float maxX = 2.2f;

	float minX = 1.6f ;

	float mouseSens = 140;

	float radius = 6.2f;

	float maxDistPerSec = 10.0f;


	float minHeightAtPoint = 0.0f;
};

