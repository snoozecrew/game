#pragma once

#include <ObjectFactory.h>


class DreamObjectFactory :public ObjectFactory
{
public:
	static DreamObjectFactory* getInstance();
	Terrain * makeTerrain(VAOData *, TerrainTextureData *, float, float);
	GameObject * makeCustomObject(VAOData*, glm::vec3 pos, glm::vec3 scale, glm::vec3 rot); 
	GameObject * makeCustomObject(VAOData *iData);
	GameObject * makeInstancedObject(InstancedVAOData *);

private:
	static  DreamObjectFactory * thisPointer;


	DreamObjectFactory();

	~DreamObjectFactory();
};

