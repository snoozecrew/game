#pragma once
#include <Object3D.h>
#include "EntityShaders.h"

class DreamCustom :public Object3D
{
public:
	DreamCustom(VAOData*, glm::vec3, glm::vec3, glm::vec3);
	~DreamCustom();
	void update(double);
	void draw(RenderSettings * rs);

private:
	EntityShaders * shaders;
};

