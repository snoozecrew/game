#pragma once

#define ATTACK_SLASH 0
#define ATTACK_STAB 1	

//Duration of attack
const static int attackTimes[2] = { 270, 500 };

//Attack cone spread, in radians
const static float attackSpreads[2] = { 1.5, 0.7 };

//Reach of the attack cone in front on player
const static float attackRadii[2] = { 2.5, 3.0 };