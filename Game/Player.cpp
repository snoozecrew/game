#include "Player.h"
#include "EntityShaders.h"
#include "EnemyPool.h"


Player::Player(glm::vec3 startPos, VAOData * newData, VAOData* arrowData, EnemyPool* pool) :AttackableObject(pool)
{
	posX = startPos.x;
	posY = startPos.y;
	posZ = startPos.z;

	setScale(glm::vec3(0.4));

	dataVAO = newData;

	arrowVAO = arrowData;

	shaders = EntityShaders::getInstance();
	defaultShaders = shaders;

	totalHealth = 180;
	health = 180;
}

void Player::update(double delta)
{
	Object3D::update(delta);

	//only passes once if the player has actually died
	if (!dead) {

		if (getHealth() <= 0) {
			std::cout << "YOU DIED\n";
			dead = true;
			death(delta);
		}
	}
	if (dead)
	{
		death(delta);
	}

	if (attacking) {
		attackTimer += delta;
		//deal the damage midway through attacking, and only once
		if (attackTimer > attackTimes[currentAttack] / 2 && !damageDealt) {

			attack();
		}
		//attack "animation" finishes
		if (attackTimer > attackTimes[currentAttack]) {

			attacking = false;
			attackTimer = 0.0f;
			//DELETE ARROW	
			delete arrow;
			arrow = nullptr;

		}
	}

	Object3D::lateUpdate(delta);
	AttackableObject::update();
	if (arrow == nullptr) return;

	//do arrow movement update
	if (attacking) {

		if (currentAttack == ATTACK_SLASH) {
			//update position on arc

			currentRads += radsPerFrame;
			float totalRads = attackSpreads[currentAttack];
			float xPos = (attackRadii[currentAttack] / 2) * sin(currentRads - (totalRads / 2));
			float zPos = (attackRadii[currentAttack] / 2) * cos(currentRads - (totalRads / 2));

			glm::vec3 newRot = getRot();

			arrowPos = getRotMat() * glm::vec4(xPos, arrowHeight, zPos, 1);
			arrowPos3 = arrowPos;
			arrowPos3 += getPos();

			glm::vec3 difference = getPos() - arrowPos3;
			newRot.y = -((M_PI /2) - atan2(difference.x, difference.z));
		
			arrow->setPos(arrowPos3);
			arrow->setRot(newRot);
		

		}
		else if (currentAttack == ATTACK_STAB) {
			//update position forward of player

			arrowPos = getRotMat() * glm::vec4(0, arrowHeight, distPerFrame * framesPassed, 1);

			arrowPos3 = arrowPos;

			arrow->setPos(getPos() + arrowPos3);
			arrow->setRot(getRot());
			
		}
		framesPassed++;
	}
	

	arrow->update(deltaTime);


}

void Player::death(double deltaTime) {
	

	//"death animation"
	deathTimer += deltaTime;
	if (deathTimer > DEATH_TIME) {

		//Do things on death

		deathTimer = 0.0;
	}

}

void Player::OnClick(int key)
{
	if (key == INPUT_LEFT_MOUSE_DOWN && attacking == false)
	{
		currentAttack = ATTACK_SLASH;
		attacking = true;
		damageDealt = false;

		genArrow();
	}
	if (key == INPUT_RIGHT_MOUSE_DOWN && attacking == false)
	{
		currentAttack = ATTACK_STAB;
		attacking = true;
		damageDealt = false;

		genArrow();
	}
}


void Player::draw(RenderSettings*rs)
{
	Object3D::draw(rs);

	if (arrow == nullptr) return;

	arrow->draw(rs);

}

void Player::attack()
{
	damageDealt = true;
	//Do the attack
	minionsInRange = pool->getAttackableMinions(getPos(), getRot(), currentAttack);

	for (int i = 0; i < minionsInRange.size(); i++) {
		minionsInRange[i]->takeDamage(attackDamages[currentAttack]);
	}
	minionsInRange.clear();
}

void Player::genArrow() {

	arrow = new DreamCustom(arrowVAO, getPos(), glm::vec3(0.06), getRot());
	arrow->setConfig(thisConfig);

	//resets
	distPerMS = 0.0f;
	distPerFrame = 0.0f;
	radsPerMS = 0.0f;
	radsPerFrame = 0.0f;

	framesPassed = 0;

	//different starting positions per attack
	if (currentAttack == ATTACK_SLASH) {

		//calculate how much the arrow will travel on each frame

		radsPerMS = attackSpreads[currentAttack] / attackTimes[currentAttack];
		radsPerFrame = (radsPerMS * deltaTime) * 2;

		currentRads = -attackSpreads[currentAttack] / 2;
		//set start pos
		float xStartPos = (attackRadii[currentAttack] / 2) * sin(attackSpreads[currentAttack] / 2);
		float zStartPos = (attackRadii[currentAttack] / 2) * cos(attackSpreads[currentAttack] / 2);
		arrow->setPos(glm::vec3(getPos().x + xStartPos, getPos().y + arrowHeight, getPos().z + zStartPos));
		arrow->setRot(glm::vec3(arrow->getRot().x, arrow->getRot().y, arrow->getRot().z));
	}
	if (currentAttack == ATTACK_STAB) {

		//stab attack starts on player pos, so no need to reset start position
		//calculate how much the arrow will travel on each frame
		distPerMS = attackRadii[currentAttack] / attackTimes[currentAttack];
		distPerFrame = distPerMS * deltaTime;
	}
	
}
