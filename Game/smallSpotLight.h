#pragma once
#include "Light.h"
class smallSpotLight : public Light
{
public:
	smallSpotLight(glm::vec3 colors, glm::vec3 pos) 
	{
		position = glm::vec4(pos, 1.0f);
		attenuation = 0.01f;
		ambientCoefficient = 0.00f;
		coneAngle = 25.0f;
		coneDirection = glm::vec3(0.0f, -1.0f, 0.0f);
		intensities = colors;
	}


};

