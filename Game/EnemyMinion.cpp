#include "EnemyMinion.h"



EnemyMinion::EnemyMinion(AttackableObject* player, VAOData* newData, glm::vec3 startPos, int level, EnemyPool* pool) :AttackableObject(pool)
{
	//Set minion's attack target
	target = player;
	targetPos = target->getPos();

	//Initialize position of minion
	setPos(startPos);

	dataVAO = newData;

	//Pick out the starting health value of the minion
	health = HEALTH[level];
	atkDamage = ATK_DAMAGE[level];

	shaders = EntityShaders::getInstance();
	defaultShaders = shaders;

	setScale(glm::vec3(scales[level]));
}

EnemyMinion::~EnemyMinion()
{
}


void EnemyMinion::update(double deltaTime) {

	Object3D::update(deltaTime);

	componentReference->physics->unSetState(PhysicsStates::PHYSICS_RESTING);

	//check minion HP
	if (getHealth() <= 0) {

		//minion died
		death(deltaTime);
	}

	//If target dead, all minions do nothing
	int targetHealth = target->getHealth();
	if (targetHealth < 0) {

		targetDead = true;
		state = MINION_STATE_STILL;
	}

	if (attackDelayTimer > 0) {

		attackDelayTimer -= deltaTime;
	}

	//check the state of the minion
	switch (state) {

	case MINION_STATE_ROAMING:
		roam(deltaTime);
		break;

	case MINION_STATE_TRAVELLING:
		travelling(deltaTime);
		break;

	case MINION_STATE_ATTACKING:
		attack(deltaTime);
		break;

	case MINION_STATE_STILL:
		still(deltaTime);
		break;

	case MINION_STATE_DYING:
		death(deltaTime);
		break;

	default:
		state = MINION_STATE_STILL;
		still(deltaTime);
		break;
	}

	AttackableObject::update();

	Object3D::lateUpdate(deltaTime);
}

void EnemyMinion::roam(double deltaTime)
{
	if (inAggressionRadius()) {
		despawnTimer = DESPAWN_TIMEOUT;
		//Player in radius, so travel towards
		state = MINION_STATE_TRAVELLING;
	}

	else {
		//rand direction
		//rand time duration of travel
		//rand pause duration


		despawnTimer -= deltaTime / 1000;
		//if timer ran out, or player is further than despawn distance
		if (despawnTimer < 0.0f || glm::length(getPos() - target->getPos()) > DESPAWN_DISTANCE)
		{
			
			pool->removeMinion(this);
			kill();
			return;
		}
	}
}

void EnemyMinion::travelling(double deltaTime)
{
	targetPos = target->getPos();
	//calculate distance between the minion and target
	glm::vec3 difference = targetPos - getPos();
	float distance = glm::length(difference);

	//calculate the rotation to face the player
	float rot = atan2(difference.x, difference.z);

	setRot(glm::vec3(0, rot, 0));

	//travel towards player
	movement = glm::vec4(0, 0, aggrMoveSpeed * deltaTime, 0);
	movement = getRotMat() * movement;
	
	movementVec3 = glm::vec3(movement.x, movement.y, movement.z);
	setPos(getPos() + movementVec3);

	
	if (!inAggressionRadius()) {

		//Player ran away far enough, so go back to roaming
		state = MINION_STATE_ROAMING;
	}
	//check if player is attackable
	if (inAttackRadius()) {

		state = MINION_STATE_ATTACKING;
	}
}

void EnemyMinion::attack(double deltaTime)
{	

	if (!inAttackCancelRadius()) {

		//Fallen out of attack radius while attempting to attack
		state = MINION_STATE_TRAVELLING;
	}

	//do attack
	attackTimer += deltaTime;
	if (attackTimer > ATTACK_TIME && attackDelayTimer <= 0) {

		//attack "animation" time passed, so deal damage
		target->takeDamage(atkDamage);

		attackTimer = 0.0;
	}
}

void EnemyMinion::still(double deltaTime) {
	
	//If target dead, do nothing, else continue
	if (!targetDead) {

		//remain until within aggression distance
		if (inAggressionRadius()) {

			state = MINION_STATE_TRAVELLING;
		}
	}
}

void EnemyMinion::death(double deltaTime)
{

	deathTimer += deltaTime;
	if (deathTimer > DEATH_TIME) {

		pool->removeMinion(this);
	}
}

//Check if minion is within aggression radius
bool EnemyMinion::inAggressionRadius() {

	targetPos = target->getPos();
	//calculate distance between the minion and target
	glm::vec3 difference = targetPos - getPos();
	float distance = glm::length(difference);

	if(distance <= AGGRESSION_RADIUS) return true;

	return false;
}

//Check if within attack radius
bool EnemyMinion::inAttackRadius()
{
	targetPos = target->getPos();
	//calculate distance between the minion and target
	glm::vec3 difference = targetPos - getPos();
	float distance = glm::length(difference);

	if (distance <= ATTACK_RADIUS) return true;

	return false;
}

//once minion is in attacking state, it may remain there until falling out of this radius
//Check if fallen out of cancellable attack radius
//return true if still within radius
bool EnemyMinion::inAttackCancelRadius()
{
	targetPos = target->getPos();
	//calculate distance between the minion and target
	glm::vec3 difference = targetPos - getPos();
	float distance = glm::length(difference);

	if (distance <= ATTACK_CANCEL_RADIUS) return true;

	return false;
}

