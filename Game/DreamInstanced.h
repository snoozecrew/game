#pragma once
#include <InstancedObject3D.h>
class DreamInstanced : public InstancedObject3D
{
public:
	DreamInstanced(InstancedVAOData* iData);
	~DreamInstanced();


	void update(double);
	void draw(RenderSettings *);
};

