#pragma once
#include "Player.h"
#include <PhysicsComponent.h>
#include <Helpers.h>
#include <VAOData.h>
#include <math.h>
#include "AttackableObject.h"
#include "EnemyPool.h"

//roaming around slowly, randomly
#define MINION_STATE_ROAMING 0
//Travelling straight to the player
#define MINION_STATE_TRAVELLING 1
//Dealing damage to the player
#define MINION_STATE_ATTACKING 2
//Staying still
#define MINION_STATE_STILL 3
//Dying animation, only 100/150ms
#define MINION_STATE_DYING 4

class EnemyMinion : public Object3D, public AttackableObject
{
public:
	EnemyMinion(AttackableObject*, VAOData*, glm::vec3, int, EnemyPool*);
	virtual ~EnemyMinion();

	void update(double);	
	void draw(RenderSettings* rs) { Object3D::draw(rs); }

	static const int AGGRESSION_RADIUS = 22;

private:
	EntityShaders * shaders;

	//Minion States
	int state = 0;
	void roam(double);
	void travelling(double);
	void attack(double);
	void still(double);
	void death(double);


	bool inAggressionRadius();
	bool inAttackRadius();
	bool inAttackCancelRadius();

	AttackableObject* target;
	glm::vec3 targetPos;

	//Arrays based on minion level
	const static int MAX_LEVELS = 3;
	const int HEALTH[MAX_LEVELS] = { 100, 150, 260 };
	const int ATK_DAMAGE[MAX_LEVELS] = { 6, 9, 14 };

	float scales[MAX_LEVELS] = { 0.7f, 1.15f, 1.4f };

	//Radii affecting minion state
	const float ATTACK_RADIUS = 1.0f;
	const float ATTACK_CANCEL_RADIUS = 2.0f;

	//Roaming despawn timing (subtractive)
	const int DESPAWN_TIMEOUT = 30;
	float despawnTimer = 30.0f;

	const int DESPAWN_DISTANCE = 50;


	//Movement
	float aggrMoveSpeed = 0.0065f;
	float roamMoveSpeed = 0.003f;

	glm::vec4 movement = glm::vec4(0);
	glm::vec3 movementVec3 = glm::vec3(0);

	//Attacking
	const int ATTACK_TIME = 500;
	double attackTimer = 0.0f;
	int atkDamage;

	bool targetDead = false;

	//Ded
	const int DEATH_TIME = 150;
	double deathTimer = 0.0f;



};

