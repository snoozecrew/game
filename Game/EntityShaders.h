#pragma once
#include <DefaultShaders.h>
#include "DreameaterShader.h"
class EntityShaders : public DreameaterShader , public DefaultShaders
{
public:
	static EntityShaders* getInstance();
	~EntityShaders();

	static void forceRebuild();


private:
	std::string vertexFilename  ="Shaders/vertex_shader_dream.shader";
	std::string fragFilename = "Shaders/fragment_shader_dream.shader";

	static EntityShaders* thisPointer;

	void setup();

	EntityShaders();
};

