#include "MainMenu.h"
#include <LightingState.h>
#include <Config.h>
#include <Game.h>
#include <texture_loader.h>
#include <ImageRect.h>


MainMenu::MainMenu()
{
	name = "MainMenu";
}


MainMenu::~MainMenu()
{
}

void MainMenu::load(Game * refr)
{
	gameRefr = refr;

	loadInit();

	GLuint smolImage = fiLoadTexture("../../sceneryassets/Textures/load.jpg");

	loadImg = new ImageRect(smolImage);

	loadImg->sortVAO();

	addGameObject(loadImg);

	menuController = new StartGame(refr);

	addPublicBehaviour(menuController);

	gameRefr->setConfigAll();
}