#pragma once
#pragma once
#include "Light.h"

class PointLight : public Light
{
public:
	PointLight(glm::vec3 pos)
	{
		position = glm::vec4(pos, 1.0f);
		attenuation = 0.06f;
		ambientCoefficient = 0.0f;
		coneDirection = glm::vec3(0.0f, 0.0f, 0.0f);
		coneAngle = 36000;
		intensities = glm::vec3(1.0f, 1.0f, 1.0f);

	}



};