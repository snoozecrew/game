#include "DreamTerrain.h"



DreamTerrain::DreamTerrain(VAOData * v, TerrainTextureData * t) : Terrain(v,t)
{
	drTerrShaders = DreamTerrainShaders::getInstance();
}


DreamTerrain::~DreamTerrain()
{
}


void DreamTerrain::update(double d)
{
	Object3D::update(d);
	Object3D::lateUpdate(d);
}

void DreamTerrain::draw(RenderSettings* rs)
{
	defaultShaders = drTerrShaders;
	defaultShadersAsTerr = drTerrShaders;

	Terrain::draw(rs);
}