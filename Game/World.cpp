#include <iostream>
#include <string>
#include "World.h"
#include <stdio.h>
#include <Game.h>
#include <Skybox.h>
#include <Scenery.h>
#include <VAOData.h>
#include <Camera.h>
#include <FirstPersonCamera.h>
#include <Config.h>
#include "KeyboardMove.h"
#include "Player.h"
#include "DreamCamera.h"
#include <FPSCounter.h>
#include <PrintPositions.h>
#include <ContrastPP.h>
#include <WaterPlane.h>
#include <GrassyTerrainTextureData.h>
#include "RoamBehaviour.h"
#include "TrackColouredArea.h"
#include <PointVolume.h>
#include "DreameaterTerrainTextures.h"
#include "SunLight.h"
#include "PlayerMovement.h"
#include "PlayerControlSwitch.h"
#include <PhysicsComponent.h>
#include <ComponentReference.h>
#include "EnemyMinion.h"
#include "EnemySpawner.h"
#include <GameObject.h>
#include "DreamObjectFactory.h"
#include "Cara.h"

World::World()
{
	name = "World";
}

World::~World()
{
}

void World::loadInit()
{
	Scene::loadInit();

	loadTextures();


	GameObject * skyBox = new Skybox(1);

	addGameObject(skyBox);

	examiner = ObjExaminer::getInstance();

	examiner->changeObjectFactory(DreamObjectFactory::getInstance());

	examiner->setDirectoryTexture("..\\..\\sceneryassets\\");
	examiner->setDirectoryObject("..\\..\\sceneryassets\\");
}

void World::run()
{
	gameRefr->captureMouse = true;
	gameRefr->setPlayer(player);
	gameRefr->setPlaying(true);
}

void World::load(Game * refr)
{
	gameRefr = refr;

	World::loadInit();

	loadLighting();

	loadWorld();

	loadEnemyPool();

	loadPlayer();

	loadEnemySpawner();

	loadCamera();

	addPlayerBehaviours();

	loadCara();

	loadRiver();

	loadGUI();

	loadPP();

	loadColourScripts();

	gameRefr->setConfigAll();

	gameRefr->getPhysicsEngine();
}

void World::loadRiver()
{
	GLuint waterDudv = fiLoadTexture("Textures/waterDUDV.png");

	GLuint waterNormal = fiLoadTexture("Textures/waterNormalMap.png");

	WaterPlane* river0 = new WaterPlane(glm::vec2(6,11));

	river0->setPos(glm::vec3(-150.0f, 65.0f, 81.0f));

	addReflectiveGameObject(river0);

	river0->setDudv(waterDudv);

	river0->setNormal(waterNormal);

}

void World::loadGUI()
{
	PrintPositions * pp = new PrintPositions(gameRefr);

	pp->addObject(player, "Player");
	addPublicBehaviour(pp);
}

void World::loadPP()
{
	//gameRefr->addPostProccessor(new ContrastPP(0.12f));
}

void World::loadCamera()
{
	gameCamera = new DreamCamera(player);
	gameRefr->setCamera(gameCamera);

	FPSCounter * fps = new FPSCounter(gameRefr);
	addGUIObject(fps);
}

void World::loadEnemyPool()
{
	minionPool = new EnemyPool();
	addPublicBehaviour(minionPool);
}

void World::loadPlayer()
{
	examiner->setDirectoryObject("..\\..\\sceneryassets\\player\\");
	//Set the file
	examiner->setFile("newPlayer.obj");

	VAOData *playerModel = new VAOData();

	examiner->indexShapes(playerModel);

	//gen arrow
	examiner->setFile("AttackArrow.obj");

	VAOData *arrow = new VAOData();

	examiner->indexShapes(arrow);

	Player * playerCustom = new Player(glm::vec3(-180, 100, 130), playerModel, arrow, minionPool);

	player = playerCustom;

	playerAsInputListener = playerCustom;

	playerAsAO = playerCustom;

	player->setPos(findWaypoint("wp_PlayerStart")->getPos());
	//Phys
	playerCustom->setVolume(new PointVolume(player, examiner->getExtents()));

	player->componentReference->physics = new PhysicsComponent(player);

	//get the stuff needed for the health bar
	GLuint barTex = fiLoadTexture("Textures/loadBar.png");
	glm::vec2 barDims;
	barDims.x = getW();
	barDims.y = getH();
	GLuint healthTex = fiLoadTexture("Textures/ProgressPlayer.png");

	//Adding the completed player to the scene
	addGameObject(playerCustom);
	//and as an input listner
	addInputListener(playerAsInputListener);
	//setup and add the health bar
	playerAsAO->setupHealthBar(barTex, healthTex, barDims);
	addTransparentGameObject(playerAsAO->getHealthBar());
}

void World::loadEnemySpawner() 
{
	//Make a vaodata for the enemy
	examiner->setDirectoryObject("..\\..\\sceneryassets\\Player\\");
	examiner->setFile("Player.obj");

	VAOData* enemyVAO = new VAOData();
	examiner->indexShapes(enemyVAO);

	//make an eenemy spawner and give it the minion pool, this scene, the player's attackableobject form and the minion VAO
	enemySpawner = new EnemySpawner(this, minionPool);
	enemySpawner->setup(enemyVAO, playerAsAO);

	//Add both behaviours

	addPublicBehaviour(enemySpawner);
}

void World::loadEnemy() {

	examiner->setDirectoryObject("..\\..\\sceneryassets\\Player\\");
	examiner->setFile("Player.obj");

	VAOData* enemyVAO = new VAOData();
	examiner->indexShapes(enemyVAO);

	EnemyMinion* enemyMinion = new EnemyMinion(playerAsAO, enemyVAO, glm::vec3(-440, 100, 300), 0, minionPool);

	enemyMinion->setVolume(new PointVolume(enemyMinion, examiner->getExtents()));

	enemyMinion->componentReference->physics = new PhysicsComponent(enemyMinion);

	addGameObject(enemyMinion);

}

void World::addPlayerBehaviours()
{
	KeyboardMove * flightController = new KeyboardMove(gameRefr);

	PlayerMovement * playerController = new PlayerMovement(gameCamera, gameRefr);

	PlayerControlSwitch * playerControlSwitch = new PlayerControlSwitch(gameRefr, playerController, flightController);

	player->addPrivateBehaviour(flightController);
	player->addPrivateBehaviour(playerController);
	player->addPrivateBehaviour(playerControlSwitch);
}

void World::loadLighting()
{
	currLighting->addLight(new SunLight(glm::vec3(0.15f,1,0)) );
}

void World::loadWorld()
{
	examiner->setDirectoryObject("..\\..\\sceneryassets\\World\\");

	examiner->setFile("World.obj");
	
	SceneConfig sc;

	sc.tilingFactor = 160;

	sc.texData = new DreameaterTerrainTextures();

	//sc.addInstancible("Tree_A");
	//sc.addInstancible("Tree_B");
	//sc.addInstancible("Log_A");
	//sc.addInstancible("Rock_A");
	//sc.addInstancible("Rock_B");
	//sc.addInstancible("Rock_Cluster_A");
	//sc.addInstancible("Rock_Cluster_B");

	examiner->indexScene(this, sc);

	
}

void World::loadColourScripts()
{
	trackBehaviour = new TrackAndReportColouredArea(player, enemySpawner, cara);

	addPublicBehaviour(trackBehaviour);

	Waypoint * focalPoint = nullptr;

	std::string focal = "wp_Focal";

	for (int count = 0; count < 6; count++)
	{
		focalPoint = nullptr;
		focalPoint = findWaypoint(focal + to_string(count+1));
		if (focalPoint != nullptr)
		{
			trackBehaviour->addFocalPoint(focalPoint->getPos());
		}
	}
}

void World::loadCara()
{
	examiner->setDirectoryObject("..\\..\\sceneryassets\\Cara\\");

	examiner->setFile("Cara.obj");

	VAOData * caraModel = new VAOData();

	examiner->indexShapes(caraModel);

	cara = new Cara(caraModel, playerAsAO);

	currLighting->addLight(cara->setupLight());

	cara->setPos(player->getPos()+glm::vec3(0,1,0));

	cara->setScale(glm::vec3(0.4));

	addTransparentGameObject(cara);
}
