#include "InstancedShader.h"


DreamInstancedShader * DreamInstancedShader::thisPointer = nullptr;

DreamInstancedShader::DreamInstancedShader()
{
	programID = setupShaders(iDreamVertFilename, iDreamFragFilename);
	setup();
	DreameaterShader::setup(programID);
}


DreamInstancedShader::~DreamInstancedShader()
{
}

DreamInstancedShader * DreamInstancedShader::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new DreamInstancedShader();
	}
	return thisPointer;
}

void DreamInstancedShader::forceRebuild()
{
}


void DreamInstancedShader::setup()
{
	InstancedShaders::setup();
}