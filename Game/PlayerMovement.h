#pragma once

#include <PrivateBehaviour.h>
#include "DreamCamera.h"
class Game;

#define PLAYER_LEFT 1
#define PLAYER_RIGHT -1
#define PLAYER_FORWARD 2
#define PLAYER_BACKWARD -2
class PlayerMovement : public PrivateBehaviour
{
public:
	PlayerMovement(Camera *,  Game * game);
	~PlayerMovement();

	void update(double);
private:

	//Changing movement values
	float playerCurrentYRot = 0.0f;
	float velocity = 0.0f;
	glm::vec4 movement = glm::vec4(0);
	glm::vec3 movement3;

	void dodge(int);

	void jump();

	//const movement values
	const float forwardAcc = 0.011f;
	const float sprintForwardAcc = 0.016f;
	const float reverseAcc = -0.0065f;
	const float strafeSpeed = 0.004f;

	const float rotationModifer = 1.1f;

	const float initialDodgeSpeed = 0.05f;
	const float initialUpwardsDodgeSpeed = 0.008f;
	const float dodgeMultiplierPerFrame = 0.99f;
	const float minimumDodge = 0.0001f;


	const float initialJumpSpeed = 0.02f;
	const float jumpMultiplierPerFrame = 0.99f;
	const float minimumJump = 0.0001f;

	float rot = 0.0f;

	bool strafing = false;

	bool dodging = false;
	glm::vec4 dodgingMomentum;

	bool jumping = false;
	glm::vec4 jumpingMomentum;

	Game * gameRefr = nullptr;
	Camera * camera;
};