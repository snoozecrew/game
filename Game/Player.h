#pragma once

#include <Object3D.h>
#include <VAOData.h>
#include <glm-0.9.9.2/glm/glm.hpp>
#include "EntityShaders.h"
#include "AttackableObject.h"
#include "AttackTypes.h"
#include <InputListener.h>
#include "DreamCustom.h"
#include <math.h>
#include <vector>

class Player : public  Object3D, public AttackableObject, public InputListener
{
public:
	Player(glm::vec3, VAOData*, VAOData*, EnemyPool*);

	void update(double);
	void draw(RenderSettings*);

	void OnClick(int key);

private:

	EntityShaders * shaders;

	DreamCustom* arrow = nullptr;
	VAOData* arrowVAO;


	void death(double);

	//Death
	bool dead = false;
	const int DEATH_TIME = 150;
	double deathTimer = 0.0f;

	//Attacking
	std::vector<AttackableObject*> minionsInRange;
	bool attacking = false;
	bool damageDealt = false;
	int attackDamages[2] = { 40, 70 };
	int currentAttack = 0;
	double attackTimer = 0.0;

	void attack();

	//Arrow
	void genArrow();
	float arrowHeight = 0.9f;
	float distPerMS = 0.0f;
	float radsPerMS = 0.0f;

	float distPerFrame = 0.0f;
	float radsPerFrame = 0.0f;
	float currentRads = 0.0f;

	glm::vec4 arrowPos;
	glm::vec3 arrowPos3;
	int framesPassed = 0;
};