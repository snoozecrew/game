#include "DreamCustom.h"



DreamCustom::DreamCustom(VAOData* v, glm::vec3 p, glm::vec3 s, glm::vec3 r)
{
	shaders = EntityShaders::getInstance();
	dataVAO = v;
	setPos(p);
	setScale(s);
	setRot(r);
}


DreamCustom::~DreamCustom()
{
}


void DreamCustom::update(double d)
{
	Object3D::update(d);

	Object3D::lateUpdate(d);
}

void DreamCustom::draw(RenderSettings * rs)
{
	defaultShaders = shaders;
	
	Object3D::draw(rs);
}