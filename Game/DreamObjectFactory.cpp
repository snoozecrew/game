#include "DreamObjectFactory.h"
#include "DreamTerrain.h"
#include "DreamInstanced.h"
#include "DreamCustom.h"

DreamObjectFactory* DreamObjectFactory::thisPointer = nullptr;

DreamObjectFactory::DreamObjectFactory()
{

}


DreamObjectFactory::~DreamObjectFactory()
{

}

DreamObjectFactory* DreamObjectFactory::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new DreamObjectFactory();
	}
	return thisPointer;
}


Terrain * DreamObjectFactory::makeTerrain(VAOData * v, TerrainTextureData * tex, float tile , float scale)
{
	DreamTerrain* terrain = new DreamTerrain(v, tex);


	terrain->setTilingFactor(tile);

	terrain->setScale(glm::vec3(scale));
	return terrain;
}

GameObject * DreamObjectFactory::makeCustomObject(VAOData * v, glm::vec3 p, glm::vec3 s, glm::vec3 r)
{
	DreamCustom * custom = new DreamCustom(v, p ,s,r);


	return custom;
}


GameObject * DreamObjectFactory::makeCustomObject(VAOData *v)
{
	DreamCustom * custom = new DreamCustom(v, glm::vec3(0), glm::vec3(1), glm::vec3(0));


	return custom;
}

GameObject * DreamObjectFactory::makeInstancedObject(InstancedVAOData * iData)
{
	DreamInstanced * inst = new DreamInstanced(iData);
	return inst;
}