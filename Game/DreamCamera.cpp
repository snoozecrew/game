#include "DreamCamera.h"

#include <Game.h>

DreamCamera::DreamCamera(GameObject* Target) 
{
	target = Target;
	cameraAngle.y = M_PI / 2 + 0.2;
	cameraAngle.x = minX + 0.3;
}


DreamCamera::~DreamCamera()
{
}


void DreamCamera::update(double delta, int mouseX, int mouseY, bool playing)
{
	float deltaTime = delta;

	cameraAngle.x += (1.0f*(mouseY)) / mouseSens;
	cameraAngle.y += (-1.0f*(glm::min(mouseX,900))) / mouseSens;


	if (cameraAngle.y > M_PI * 2) cameraAngle.y -= M_PI * 2;
	if (cameraAngle.y < -M_PI * 2) cameraAngle.y += M_PI * 2;

	if (cameraAngle.x > maxX)
	{
		cameraAngle.x = maxX - 0.001f;
	}
	else if (cameraAngle.x < minX)
	{
		cameraAngle.x = minX + 0.001f;
	}

	///<Summary>
	///1. The first step calculates the new camera position
	///</Summary>

	if (target != nullptr)
	{
		playerPos = target->getPos();	
	}

	float s = cameraAngle.y;
	float t = cameraAngle.x;

	targetCameraPos = playerPos;

	targetCameraPos.z -= radius * cos(s) * sin(t);
	targetCameraPos.x -= radius * sin(s) * sin(t);
	targetCameraPos.y -= radius * cos(t);



	//cameraPos = cameraPos + ((maxDistPerSec / 1000.0f)*deltaTime)* targetCameraPos;
	cameraPos = targetCameraPos;

	///<Summary>
	///2. The second step calculates the look target
	///</Summary>

	cameraTarget =playerPos;


	///<Summary>
	///3. Calculate the camera matrix and mulitply it by the perspective matrix
	///</Summary>

	RayCastResult r = Game::getInstance()->getPhysicsEngine()->castRayDown(cameraPos);

	if (r.hitPos.y > 30)
	{
		minHeightAtPoint = r.hitPos.y + 0.14f;
	}


	if (cameraPos.y < minHeightAtPoint)
	{
		cameraPos.y = minHeightAtPoint;
	}

	cameraNoPersp= glm::lookAt(cameraPos, cameraTarget, glm::vec3(0,1,0));

	camera = persp * cameraNoPersp;

	///<Summary>
	///5. Decay the totalY and totalX rot values
	///</Summary>


	totalYRot *= yDecay;
	totalXRot *= xDecay;
}


glm::mat4 DreamCamera::getLookAtMatrix(bool invertY, glm::vec3 pos)
{
	if (invertY)
	{

		//Use case - water reflections
		//pos is already lower, we just need to work out a new camera target

		glm::vec3 lookVector = cameraPos - cameraTarget;

		lookVector.y = -lookVector.y;

		
		return persp * glm::lookAt(pos, pos - lookVector, glm::vec3(0, 1, 0));
	}
	else
	{
		std::cout << ("THIS DOES NOT WORK YETz\n");
	}

}