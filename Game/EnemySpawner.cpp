#include "EnemySpawner.h"
#include <Scene.h>
#include <PointVolume.h>
#include <TerrainCollider.h>



EnemySpawner::EnemySpawner(Scene* s, EnemyPool* p): scene(s), pool(p)
{
}


EnemySpawner::~EnemySpawner()
{
}

void EnemySpawner::setup(VAOData * minion, AttackableObject* newPlayer)
{

	player = newPlayer;
	minionVAO = minion;

	barTex = fiLoadTexture("Textures/loadBar.png");
	barDims.x = getW();
	barDims.y = getH();
	healthTex = fiLoadTexture("Textures/progress.png");

	//fetch locations of spawner waypoints
	//this can be done nicer if there were more points.. simple currently due to time constraints
	minionSpawn1 = scene->findWaypoint("wp_MinionSpawn1")->getPos();
	minionSpawn2 = scene->findWaypoint("wp_MinionSpawn2")->getPos();

	MINION_SPAWN_POINTS.push_back(minionSpawn1);
	MINION_SPAWN_POINTS.push_back(minionSpawn2);
	MINION_SPAWN_POINTS.push_back(scene->findWaypoint("wp_MinionSpawn3")->getPos());
	MINION_SPAWN_POINTS.push_back(scene->findWaypoint("wp_MinionSpawn4")->getPos());
	MINION_SPAWN_POINTS.push_back(scene->findWaypoint("wp_MinionSpawn5")->getPos());
	MINION_SPAWN_POINTS.push_back(scene->findWaypoint("wp_MinionSpawn6")->getPos());

}

void EnemySpawner::update(double deltaTime)
{
	
	timer += deltaTime;
	spawnPointTimer += deltaTime;
	//Dont spawn random minions when close to minion cap
	if (timer > TIME_BETWEEN_SPAWNS && pool->getCurrNumOfMinions() < pool->MAX_MINIONS - 8)
	{
		//Get random position within correct ranges
		int x = randomXInSpawnRange();

		int z = randomZInSpawnRange();

		//make the spawn position
		glm::vec3 spawnPos = player->getPos() + glm::vec3(x, 0, z);

		//get terrain y at pos 
		spawnPos.y = scene->terrain->componentReference->terrainCollider->checkHeightAtPoint(spawnPos).height;

		//Spawn the enemy with random level between 0 and 2, position, and as a random spawned one
		spawnEnemy(rand() % 3, spawnPos);

		timer = 0.0;
	}

	//check if player is within spawner range every time the timer passes. If so, spawn enemies
	//also, check if spawn points are currently enabled
	if (spawnPointTimer > TIME_BETWEEN_SPAWNER_SPAWNS && pool->getCurrNumOfMinions() < pool->MAX_MINIONS) {

		inSpawnerRadius(player);
		spawnPointTimer = 0.0;
	}


	//TODO
	//Check if within range of designated spawn points
	//spawn them in intervals of x seconds while altar not captured.

}

//generate rand position within aggression radius, 
//then add on or subtract extra spawn radius depending on +ve or -ve result
//closest a minion can possibly spawn is x: -16, z: -16, making dist to player 22.62 (which is > aggression radius)
//therefore minion will never spawn and be instantly aggressive

int EnemySpawner::randomXInSpawnRange() {

	//Calc random x position within an area further than aggression radius, and less than max dist
	int xPos = rand() % (2 * EnemyMinion::AGGRESSION_RADIUS) - EnemyMinion::AGGRESSION_RADIUS;
	if (xPos <= 0) {
		xPos = xPos - EXTRA_SPAWN_RADIUS;
		return xPos;
	}
	else {
		xPos = xPos + EXTRA_SPAWN_RADIUS;
		return xPos;
	}

}
int EnemySpawner::randomZInSpawnRange() {

	//Same for z position
	int zPos = rand() % (2 * EnemyMinion::AGGRESSION_RADIUS) - EnemyMinion::AGGRESSION_RADIUS;
	if (zPos <= 0) {
		zPos = zPos - EXTRA_SPAWN_RADIUS;
		return zPos;
	}
	else {
		zPos = zPos + EXTRA_SPAWN_RADIUS;
		return zPos;
	}
	return 0;
}

//Check if the player is within range of the spawn points
void EnemySpawner::inSpawnerRadius(AttackableObject* target) {

	//iterate through spawn points, checking if within aggression radius
	for (int i = 0; i < MINION_SPAWN_POINTS.size(); i++) {

		if (unlock[i / 2]) continue;

		glm::vec3 difference = target->getPos() - MINION_SPAWN_POINTS.at(i);

		float distance = glm::length(difference);
		//if within, spawn lv 1 minions
		if (distance <= EnemyMinion::AGGRESSION_RADIUS) {


			spawnEnemy(0, MINION_SPAWN_POINTS.at(i));
			
		}
	}

}



void EnemySpawner::spawnEnemy(int level, glm::vec3 pos)
{
	EnemyMinion* minion = new EnemyMinion(player, minionVAO, pos, level, pool);

	minion->setVolume(new PointVolume(minion,minionVAO->getExtents(0)));

	minion->componentReference->physics = new PhysicsComponent(minion);

	pool->addMinion(minion);

	scene->addGameObject(minion);

	scene->addTransparentGameObject(minion->setupHealthBar(barTex, healthTex, barDims));
}

