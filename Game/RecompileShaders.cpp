#include "RecompileShaders.h"
#include <Game.h>
#include "EntityShaders.h"
#include "DreamTerrainShaders.h"
#include "InstancedShader.h"

RecompileShaders::RecompileShaders()
{
}


RecompileShaders::~RecompileShaders()
{
}

void RecompileShaders::update(double)
{

	Keystate keys = refr->getKeys();


	if (keys & Keys::F)
	{
		EntityShaders::forceRebuild();
		DreamTerrainShaders::forceRebuild();
		DreamInstancedShader::forceRebuild();
	}
}
