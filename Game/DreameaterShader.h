#pragma once
#include "TrackColouredArea.h"
#include <DefaultShaders.h>
#include <vector>

class DreameaterShader
{
public:
	DreameaterShader() {}
	~DreameaterShader() {}

	void setBwMix(float);


	void setCircles(std::vector<glm::vec3>);

	void setFocalPoints(std::vector<glm::vec4>);

	static const int MAX_CIRCLES = 80;

	void setCaraPos(glm::vec3);


protected:
	void setup(int x);
	GLuint locBWmix;
	int programIDD = 0;
	GLint locCara = 0;

	GLuint locCircles[MAX_CIRCLES];

	GLuint locFocals[TrackAndReportColouredArea::MAX_FOCAL_POINTS];
};

