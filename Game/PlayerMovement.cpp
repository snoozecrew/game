#include "PlayerMovement.h"
#include <GameObject.h>
#include <ComponentReference.h>
#include <PhysicsComponent.h>
#include <Game.h>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
PlayerMovement::PlayerMovement(Camera* c, Game * game)  : camera(c), gameRefr(game)
{

}

PlayerMovement::~PlayerMovement()
{

}

void PlayerMovement::dodge(int direction)
{
	if (!dodging)
	{
		dodging = true;
		if (abs(direction / 2) == 1)//forward and back
		{
			dodgingMomentum = glm::vec4(0, initialUpwardsDodgeSpeed, direction/2 * initialDodgeSpeed, 0);
		}
		else // sidestep
		{
			dodgingMomentum = glm::vec4(direction * initialDodgeSpeed, initialUpwardsDodgeSpeed, 0, 0);
		}
		
	}
}

void PlayerMovement::jump()
{
	if (!jumping)
	{
		jumping = true;
		jumpingMomentum = glm::vec4(0, initialJumpSpeed, 0, 0);
	}
}

void PlayerMovement::update(double d)
{
	//Do shit
	if (active) {
		Keystate currKeys = gameRefr->getKeys();
		PhysicsComponent* physics = target->componentReference->physics;

		movement = glm::vec4(0);
		strafing = false;

		//Moving forward
		if (currKeys & Keys::Up)
		{
			//Sprinting?
			if (currKeys& Keys::Shift)
			{
				movement += glm::vec4(0, 0, sprintForwardAcc *d, 0);
			}
			else
			{
				movement += glm::vec4(0, 0, forwardAcc *d, 0);
			}
		}

		//Walk Backwards
		if (currKeys & Keys::Down)
		{
			movement += glm::vec4(0,0,reverseAcc *d,0);
		}


		if (physics->queryState(PhysicsStates::PHYSICS_SUPPORTED)) {
			//dodging or strafing
			if (currKeys & Keys::Left && currKeys & Keys::Space)// && currKeys & Keys::Ctrl
			{
				dodge(PLAYER_LEFT);
			}
			//doding or strafing right
			else if (currKeys & Keys::Right && currKeys & Keys::Space )// && currKeys & Keys::Ctrl
			{
				dodge(PLAYER_RIGHT);
			}
			else if (currKeys & Keys::Down && currKeys & Keys::Space)// && currKeys & Keys::Ctrl
			{
				dodge(PLAYER_BACKWARD);
			}
			else if (currKeys & Keys::Up && currKeys & Keys::Space)// && currKeys & Keys::Ctrl
			{
				dodge(PLAYER_FORWARD);
			}
			//Jumping
			else if (currKeys & Keys::Space )
			{
				jump();
			}
		}

		if (currKeys & Keys::Right && !dodging)
		{
			movement += glm::vec4(d*-strafeSpeed, 0, 0, 0);
			strafing = true;
		}
		else if (currKeys & Keys::Left && !dodging)
		{
			movement += glm::vec4(d*strafeSpeed, 0, 0, 0);
			strafing = true;
		}

		//Apply any dodging velocities
		if (dodging)
		{
			movement += dodgingMomentum *(float)d;
			dodgingMomentum *= pow(dodgeMultiplierPerFrame, d);
			if (glm::length( dodgingMomentum )< minimumDodge)
			{
				dodgingMomentum = glm::vec4(0);
				dodging = false;
			}
		}

		//Apply any jumping velocities
		if (jumping)
		{
			movement += jumpingMomentum * (float)d;
			jumpingMomentum *= pow(jumpMultiplierPerFrame, d);
			if (glm::length(jumpingMomentum)< minimumJump)
			{
				jumpingMomentum = glm::vec4(0);
				jumping = false;
				target->componentReference->physics->unSetState(PhysicsStates::PHYSICS_RESTING);
			}
		}

		//Calculate the actual position
		velocity = glm::length(movement);

		//Turn towards camera forwards over time
		//What is camera forwards?
		rot = camera->getCameraAngle().y;

		glm::vec3 currRot = target->getRot();
		//set the rotation to the new value
		target->setRot(glm::vec3(currRot.x, rot, currRot.z));


		if (velocity > 0.0f)
		{
			target->componentReference->physics->unSetState(PhysicsStates::PHYSICS_RESTING);
		}
		target->componentReference->physics->unSetState(PhysicsStates::PHYSICS_RESTING);
		//multiply the movement by the rotation matrix
		movement = glm::toMat4(glm::quat(glm::vec3(0, rot, 0))) * movement;
		//convert to vec3
		movement3 = glm::vec3(movement.x, movement.y, movement.z);
		//apply new position
		target->setPos(target->getPos() + (movement3));
	}

}


