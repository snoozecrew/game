#include "Cara.h"
#include <smallSpotLight.h>
#include <PhysicsComponent.h>
#include <random>
#include <Game.h>

Cara::Cara(VAOData * caraModel, AttackableObject * p) :player(p)
{
	dataVAO = caraModel;
	flyHeight = FLY_HEIGHT;
	state = CARA_STILL;


}


Cara::~Cara()
{
	
}

void Cara::update(double x)
{
	newRot = glm::vec3(0);
	movement = glm::vec3(0);

	Object3D::update(x);
	deltaTime = x;
	caraLight->finalMatrix = getTransMat();

	switch (state)
	{
	case CARA_STILL:
		still();
		break;
	case CARA_MOVING_TO_TARGET:
		moving();
		break;
	case CARA_CATCHING_UP_WITH_PLAYER:
		catchUp();
		break;
	case CARA_HEALING:
		heal();
		break;
	default:
		break;
	}

	glm::vec3 pos = getPos();

	pos += movement;
	pos.y = calculateHeight();

	setPos(pos);
	if (state != CARA_STILL)
	{
		if (glm::length(movement) != 0)
		{
			newRot.y = atan2(movement.x, movement.z);
		}
		else
		{
			newRot.y = lastY;
		}
	}
	setRot(newRot);
	getFlutterPos();

	lastY = newRot.y;


	glm::vec3 diff = getPos() - player->getPos();


	if (glm::length(diff) < passiveHealRadius && ticker>1000.0)
	{
		player->addHealth(2);
		ticker = 0.0;
	}

	ticker += deltaTime;

	Object3D::lateUpdate(x);

	timeSinceLastHeal += deltaTime / 1000;
}

Light * Cara::setupLight()
{
	caraLight = new Light();

	caraLight->intensities =  glm::vec3(3.5f, 3.6f, 3.0f);

	caraLight->attenuation = 0.008f;

	caraLight->position = glm::vec4(0, 0, 0, 1.0f);

	caraLight->coneAngle = 360.0f;

	caraLight->ambientCoefficient = 0.0f;

	caraLight->coneDirection = glm::vec3(0, 1, 0);

	if (caraLight != nullptr) caraLight->finalMatrix = getTransMat();

	return caraLight;
}

void Cara::startStill()
{
	state = CARA_STILL;
	stillTimer = 3500.0f + (rand() & 2000);
	std::cout << "Cara starting still\n";
}


void Cara::still()
{
	if (stillTimer <= 0.0f)
	{
		//Pick a destination

		// Too far from player?
		if (glm::length(player->getPos() - getPos()) > maxDistPlayer)
		{
			startCatchUp();
		}

		//if not
		else 
		{
			startMoving();		
			if (player->getHealth() < 90)
			{
				startHeal();
			}
		}
	}



	stillTimer -= deltaTime;
	newRot += getFlutterRot();

	glm::vec3 diff = player->getPos() - getPos();
	newRot.y = atan2(diff.x, diff.z);

}

void Cara::startMoving()
{
	state = CARA_MOVING_TO_TARGET;
	destinationPos = player->getPos() + glm::vec3(0, 2.5f, 0.5f) + glm::vec3((rand() & 15), 0, (rand() & 15));//choose random destination and calulate the height
	std::cout << "Cara starting randomPos\n";
}

void Cara::moving()
{
	//move to target
	glm::vec3 diff = destinationPos - getPos();
	float distance = glm::length(diff);

	
	movement += diff * (float)(MAX_SPEED * deltaTime);
	
	if (glm::length(movement)<MAX_SPEED_DISTANCE)
	{
		startStill();
	}

	newRot += getFlutterRot();
}

void Cara::startCatchUp()
{
	destinationPos = player->getPos() + glm::vec3(0, 2.5f, 0.5f);
	state = CARA_CATCHING_UP_WITH_PLAYER;
	std::cout << "Cara starting catch up\n";
}

void Cara::catchUp()
{
	//move to target
	glm::vec3 diff = destinationPos - getPos();
	float distance = glm::length(diff);
		

	movement += diff * (float)(MAX_SPEED * deltaTime);
	
	if (glm::length(movement)< MAX_SPEED_DISTANCE)
	{
		if (player->getHealth() < 90)
		{
			startHeal();
		}
		else
		{
			startStill();
		}
		
	}

	newRot += getFlutterRot();
}

void Cara::startHeal()
{
	if (timeSinceLastHeal < timeBetweenHeals) startStill();

	healTimer = 0.0f;
	state = CARA_HEALING;
	targetHeight = startHeightDiff;

	std::cout << "Cara starting heal\n";
}

void Cara::heal()
{
	glm::vec3 playerPos = player->getPos();

	float x = healRadius * cos(healAngle);
	float z = healRadius * sin(healAngle);

	glm::vec3 targetPos = playerPos + glm::vec3(x, 0, z);

	targetPos.y = getPos().y;

	float portion = ((deltaTime / 1000.0f) / healTime);

	targetHeight += totalHeightIncrease *portion ;

	flyHeight += (targetHeight - flyHeight) * (0.01f * deltaTime);

	healAngle += healAnglePerSec * (deltaTime / 1000.0f);
	healTimer += deltaTime / 1000.0f;

	if (healTimer > healTime)
	{
		startStill();
		flyHeight = FLY_HEIGHT;

		timeSinceLastHeal = 0.0f;

		player->addHealth(healAmount);
	}

	glm::vec3 diff = targetPos - getPos();

	movement += diff * (float)(MAX_SPEED*3 * deltaTime);
}

void Cara::getFlutterPos()
{
	if (up)
	{
		currSinSwing += deltaTime / (1000 * swingTime);
	}
	else
	{
		currSinSwing -= deltaTime / (1000 * swingTime);
	}


	float y = sin(currSinSwing);

	y *= flutterHeight;

	setPos(getPos()+ glm::vec3(0,y,0));
}

glm::vec3 Cara::getFlutterRot()
{	
	glm::vec3 x = glm::vec3(0);


	if (thisSwing < msPerSwing)
	{
		float portion = (thisSwing / msPerSwing);
		//In swing
		if (right)
		{
			x.z = (-0.5f*zSwingDegs + (portion *(zSwingDegs))) ;
			x.x = (-0.5f*xSwingDegs + (portion *(xSwingDegs))) ;
		}
		else
		{
			x.z = (0.5f*zSwingDegs - (portion *(zSwingDegs)));
			x.x = (0.5f*xSwingDegs - (portion *(xSwingDegs)));
		}
	}

	if (thisSwing >= msPerSwing)
	{
		thisSwing = 0;
		right = !right;
	}

	thisSwing += deltaTime;


	x.z *= (M_PI / 180);
	x.x *= (M_PI / 180);
	return x;
}

float Cara::calculateHeight()
{
	RayCastResult r = Game::getInstance()->getPhysicsEngine()->castRayDown(getPos());
	return r.hitPos.y + flyHeight;
}
 