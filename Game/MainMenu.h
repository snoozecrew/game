#pragma once
#include <Scene.h>
#include <LightingState.h>
#include <Config.h>
#include <Game.h>
#include <texture_loader.h>
#include "StartGame.h"

#include <ImageRect.h>

class MainMenu : public Scene
{
public:
	MainMenu();
	~MainMenu();


	void run() {}
	void loadTextures() {}
	void load(Game* refr);

private:
	ImageRect * loadImg;

	StartGame * menuController;
	
};

