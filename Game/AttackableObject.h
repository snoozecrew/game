#pragma once
#include <GameObject.h>
#include <FloatingHealthBar.h>
class EnemyPool;
class AttackableObject :public virtual GameObject
{
public:
	AttackableObject(EnemyPool*);
	virtual ~AttackableObject();

	void takeDamage(int);
	int getHealth();

	FloatingHealthBar* setupHealthBar(GLuint barTex, GLuint progTex, glm::vec2 barDim);
	void update();

	void addHealth(float heal) 
	{ health += heal;
	if (health > totalHealth) health = totalHealth;
	}

	FloatingHealthBar* getHealthBar() { return healthBar; }
private:

	const int ATTACK_DELAY = 100;


protected:
	float health = 100;
	float totalHealth = 0;
	EnemyPool* pool;	

	double attackDelayTimer = 0.0;
	FloatingHealthBar *healthBar = nullptr;
};

